<?php
/**
Template Name: Test Quesitons Page	
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage tbvets
 */

/* get_header(); ?>

	<div id="main" class="content-trails">

		<section class="supporting">
			<h1>Ask a Vet</h1>
			<p>Pet ownership isn&#8217;t always easy &mdash; but we&#8217;re here to help. Noticed a behavior change in your animal recently? Not sure if you need to bring them in for a visit, or just wait it out? Send us your questions.</p>

			<p><img src="/wp-content/themes/tbvets/images/staff.jpg" width="225" /></p>
			
			<h3>Ask the vets your question</h3>
			<?php 
			$captcha = set_session_captcha ();
			if(isset($_POST['submit'])) 
			{ 
				$data = array(
					'post_title' => $_POST['qtitle'], 
					'email' => $_POST['qemail'], 
					'name' => $_POST['qname'],
					'boogie' => $_POST['boogie']
				);
				add_question($data);			
			}
			?>
			<form action="" method="post">
			<textarea name="qtitle"></textarea>
			<p><input type="text" name="qname" value="Your name" /></p>
			<p><input type="text" name="qemail" value="your@email.com" /></p>
			<p><span style="display:block;">Please enter '<?php echo $captcha; ?>' below:</span>
			<input type="text" name="boogie" value="" /></p>
			<input type="submit" name="submit" value="Send" class="green-button" />
			</form>
		</section>
		

		<div class="content">
	        <div id="filters">
	        <h3>Search Questions</h3>
				<?php 
				if(isset($_GET['q_submit'])) { $q = $_GET['q_search']; }
				if(get_query_var('quecat')) { $qcat = get_query_var('quecat'); }
				$qcat = (strstr($qcat,'/'))?explode('/',$qcat):$qcat;
				if(is_array($qcat)) {
					$paged = ($qcat[1] != 'page')?$qcat[1]:$qcat[2];
					$qcat = ($qcat[0]!='page')?$qcat[0]:'';
				}
				?>
				<form id="q-search" action="<?php self_link(); ?>" method="get" id="searchform">
					<input type="text" name="q_search" value="Search questions..." onfocus="clearText(this)" />
					<input type="submit" class="green-button" value="Search" name="q_submit" />
				</form>

				<h3>Browse Categories:</h3>
				<ul class="categories">
				<li><a href="<?php bloginfo('url'); ?>/ask-a-vet/" title="show all" >All</a></li>

					<?php $terms = get_terms('qcat', array('hide_empty' => false)); ?>
					<?php foreach($terms as $term) : ?>	
						<li><a href="<?php bloginfo('url'); ?>/ask-a-vet/<?php echo $term->slug; ?>/" title="filter for this term" ><?php echo $term->name; ?></a></li>
					<?php endforeach; ?>
				</ul>
			</div>
			
			<h2>Browse Questions:</h2>
			<?php 
			$paged = (isset($paged)) ? $paged : 1;
			query_posts("post_type=questions&s=$q&qcat=$qcat&paged=$paged&post_per_page=10");			
			if(have_posts()):
					global $wp_query;
					//do pagination
					if($wp_query->found_posts > 10) {
					$big = intval($wp_query->found_posts.'000');	
					echo paginate_links( array(
						'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $wp_query->max_num_pages
						) );
					}
				while(have_posts()) : the_post(); 
				?>
				<div class="question">			
					<h3>Q. <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
					<div class="question-answer"><?php the_excerpt(); ?></div>
				</div>
			<?php endwhile; endif; ?>
			
			<?php if (  $blog->max_num_pages > 1 ) : ?>
				<div id="nav-below" class="navigation">
					<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts') ); ?></div>
					<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>') ); ?></div>
				</div><!-- #nav-below -->
			<?php endif; ?>

		</div><!-- #content -->

<?php get_footer(); ?>