<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage tbvets
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <title><?php wp_title(); ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="google-site-verification" content="JefZv0kpdebWeZ1STugXlWK6_XnZLj5AG96DL5lRh4c" />
    <meta name="robots" content="index, follow" />
    <meta content="Tampa Vet with six Tampa locations in Westchase / Town-n-Country, Temple Terrace, New Tampa, Lutz, and South Tampa.  Full-service animal hospital.  High-quality, top-of-the-line diagnostics and medial facilities, caring and compassionate staff." name="description">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css" />
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />

    <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_directory' ); ?>/css/normalize.min.css?ver=<?php echo rand(111,999)?>">
    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_directory' ); ?>/css/main.css?ver=<?php echo rand(111,999)?>">


    <script src="https://use.fontawesome.com/6eb3604f8d.js"></script>
    <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <script src="http://use.typekit.com/xlu2tzd.js"></script>
    <script>try{Typekit.load();}catch(e){}</script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-8942809-5', 'auto');
        ga('send', 'pageview');
    </script>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->
<div class="big-menu flex justify-center">
    <div class="flex-column">
    <h1>Menu</h1>
    <?php wp_nav_menu( array( 'menu' => 'big-menu', 'container' =>'ul' ) ); ?>
    </div>
</div>
<div id="back"></div>


<div id="wrapper" class="flex">

<!--    <div id="google_translate_element" style="background:#CBE1E8; float: left;"></div><script type="text/javascript">-->
<!--        function googleTranslateElementInit() {-->
<!--            new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');-->
<!--        }-->
<!--    </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>-->

    <div class="side-menu flex-column">
        <span class="toggle">
    			</span>
        <div class="socials flex-column">
            <a target="_blank" class="facebook" href="https://www.facebook.com/AnimalHospitalsTampa">
                <i class="icon-facebook " aria-hidden="true"></i>
            </a>
            <a target="_blank" class="twitter" href="http://www.twitter.com/tampabayvets">
                <i class="icon-twitter " aria-hidden="true"></i>
            </a>
            <a target="_blank" class="youtube" href="https://www.youtube.com/user/TampaBayVets">
                <i class="icon-youtube " aria-hidden="true"></i>
            </a>
            <a target="_blank" class="instagram" href="https://www.instagram.com/tampabayvets/">
                <i class="icon-instagram " aria-hidden="true"></i>
            </a>
            &nbsp;</div>
        <div id="slide-down"></div>

    </div>
    <div class="inner-container">
    <header role="banner" id="header">
        <nav role="navigation" class="flex justify-between">
            <h1><a href="<?php bloginfo( 'url' ); ?>" title="Tampa Animal and Bird Hospital Homepage">Tampa Animal and Bird Hospital</a></h1>

            <div class="menu flex">
                <?php wp_nav_menu( array( 'menu' => 'primary', 'container' =>'ul' ) ); ?>
                <a class="booking" href="tel:111111111">Book Now</a>
            </div>
        </nav>

    </header><!-- end header/banner -->
