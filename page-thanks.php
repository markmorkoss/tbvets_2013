<?php
/**
 Template Name: Thank you page
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

		
		<div id="main">
			<div class="content">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1><?php the_title(); ?></h1>
                    
					<div class="entry-content">
						<?php the_post_thumbnail('medium',array('class' => 'alignright frame')); ?>
						<?php the_content(); ?>
						<?php 
						$loc = $_GET['loc'];
						$loc = urldecode(sanitize_title($loc));
						?>
						<strong><a class="button" href="/what-to-expect/<?php echo $loc; ?>/">CLICK HERE to Learn What To Expect and Access the Forms you will need to fill out.</a></strong>
					</div><!-- .entry-content -->

					
				</div><!-- #post-## -->


<?php endwhile; // end of the loop. ?>

			</div><!-- #content -->

		<?php get_sidebar(); ?>
	
<?php get_footer(); ?>