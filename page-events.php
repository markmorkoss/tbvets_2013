<?php
/**
Template Name: Events Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

	<div id="main" class="content-leads">
		<div class="content">
			<?php
			global $post;
			query_posts( 'page_id=' . $post->ID );
			while ( have_posts() ) :
				the_post();
			?>
				<h1><?php the_title(); ?></h1>
				<?php the_post_thumbnail( 'medium', array( 'class' => 'alignright frame' ) ); ?>
				<?php the_content(); ?>
			<?php
			endwhile;
			wp_reset_postdata();
			?>

		</div><!-- #content -->

		<section class="supporting">
			<h1>Upcoming Events</h1>
			<?php
			$today = date( 'Y-m-d' );
			$args  = array(
				'post_type'  => 'events',
				'meta_query' => array(
					array(
						'key'      => 'simplr_end_date',
						'value'    => $today,
						'compare'  => '>=',
					),
				),
			);
			$query = new WP_Query( $args );
			if ( $query->have_posts() ) :
				while ( $query->have_posts() ) :
					$query->the_post();
					$title = get_the_title( $post->ID );
					$link = get_permalink( $post->ID );
					$start = get_post_meta( get_the_ID(), 'simplr_start_date', true );
					$end = get_post_meta( get_the_ID(), 'simplr_end_date', true );
					$start_date = date( 'F d, Y', strtotime( $start ) );
					$end_date = date( 'F d, Y', strtotime( $end ) );
					$meta = get_post_meta( $post->ID );
					echo '<div class="event" style="border-bottom:1px solid #999;">';

					/**
					 * // echo 'today'.$today.'<br>';
					 * // echo 'start '.$start.'<br>';
					 * // echo print_r($meta);
					 */

					if ( has_post_thumbnail( $post->ID ) ) :
						echo '<a href="' . esc_url( $link ) . '">';
							echo get_the_post_thumbnail(
								$post->ID,
								array( 245, 250 ),
								array(
									'class' => 'frame',
									'title' => $title,
									'alt' => $title,
								)
							);
						echo '</a>';
					endif;
					echo '<div class="clear clearfix"></div>';
					echo '<span class="start-date" style="margin-top:20px;">';
					if ( $start_date !== $end_date ) :
						echo esc_html( $start_date ) . ' - ' . esc_html( $end_date );
					else :
						echo esc_html( $start_date );
					endif;
					echo '</span>';
					echo '<h2 style="margin-top:0;">';
						echo '<a href="' . esc_url( $link ) . '" title="' . esc_attr( $title ) . '">';
							echo esc_html( $title );
						echo '</a>';
					echo '</h2>';
					echo '</div><!-- event -->';
				endwhile;
				endif;
			wp_reset_postdata();
			?>

		</section><!-- #sidebar -->

<?php get_footer(); ?>
