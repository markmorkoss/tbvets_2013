<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage tbvets
 */
?>
</div>
</div><!-- end main -->

<footer class="flex-column">

    <div class="menus flex justify-around">
        <div class="footer_menu">
            <h3>Get in Touch</h3>
            <?php wp_nav_menu( array( 'menu' => 'get-in-touch', 'container' =>'ul' ) ); ?>
        </div>
        <div class="footer_menu">
            <h3>Locations</h3>
            <?php wp_nav_menu( array( 'menu' => 'locations', 'container' =>'ul' ) ); ?>
        </div>
        <div class="footer_menu">
            <h3>Home Delivery Stores</h3>
            <?php wp_nav_menu( array( 'menu' => 'home-delivery-stores', 'container' =>'ul' ) ); ?>
        </div>
        <div class="footer_menu">
            <h3>For the pets</h3>
            <?php wp_nav_menu( array( 'menu' => 'for-the-pets', 'container' =>'ul' ) ); ?>
        </div>
    </div>
<div class="socials flex justify-center">
    <a target="_blank" class="facebook" href="https://www.facebook.com/AnimalHospitalsTampa">
        <i class="icon-facebook" aria-hidden="true"></i>
    </a>
    <a target="_blank" class="twitter" href="http://www.twitter.com/tampabayvets">
        <i class="icon-twitter" aria-hidden="true"></i>
    </a>
    <a target="_blank" class="youtube" href="https://www.youtube.com/user/TampaBayVets">
        <i class="icon-youtube" aria-hidden="true"></i>
    </a>
    <a target="_blank" class="instagram" href="https://www.instagram.com/tampabayvets/">
        <i class="icon-instagram" aria-hidden="true"></i>
    </a>
</div>
</footer>
</div>

<?php wp_footer(); ?>
</body>
</html>
