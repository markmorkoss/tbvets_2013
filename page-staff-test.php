<?php
/**

 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage tbvets
 */
global $wp_query;
if(is_numeric(@$_GET['lid'])) {
//	$location_id = preg_match('^[d+/]',$_GET['lid']);
			global $wpdb;
			$loc_name = $wpdb->get_var($wpdb->prepare("SELECT post_name FROM $wpdb->posts WHERE 1 = 1 AND ID = %d",$_GET['lid']));
			wp_redirect('/staff/'.$loc_name.'/');
}
$locations_array = array("pebble-creek","cat-doctors",'north-bay','temple-terrace');
$loc_id = get_query_var('lid');
$loc_id = (strstr($loc_id,'/'))?explode('/',$loc_id):$loc_id;
if(is_array($loc_id)) {
	$paged = ($loc_id[1] != 'page')?$loc_id[1]:$loc_id[2];
	$loc_id = ($loc_id[0]!='page')?$loc_id[0]:'';
}
if(!in_array($loc_id,$locations_array)) {
	include('single-staff.php');
	exit;
} else {
	if(is_user_logged_in()) {
		//print_r($wp_query->query_vars);
}
get_header();
?>
		<div id="main" class="content-trails">
		<section class="supporting">
		<?php if($loc_id) { ?>
					<h1>Location:<br /><?php echo $loc_name->post_title; ?></h1>
					<p><a href="<?php bloginfo('url'); ?>/staff/" title="Back to full staff list">&larr; Back to all staff</a></p>

					<div class="intro">
							<a href="<?php bloginfo('url'); ?>/location/<?php echo $loc_name->post_name; ?>/" title="<?php echo $loc_name->post_title; ?>" ><?php echo get_the_post_thumbnail($loc_name->ID, array(160,200), array('class'=>'frame')); ?></a>
							</div>

							<div class="info">
							<strong><a href="<?php bloginfo('url'); ?>/location/<?php echo $loc_name->post_name; ?>/"><?php echo $loc_name->post_title; ?></a> <span class="tiny">(<?php echo get_post_meta($loc_name->ID, 'tbvets_subtitle', true); ?>)</span></strong>
							<p class="address"><?php echo get_post_meta($loc_name->ID,'tbvets_address1',true); ?>
							<?php if((get_post_meta($loc_name->ID,'tbvets_address2',true))) { ?><?php echo get_post_meta($loc_name->ID,'tbvets_address2',true); ?><?php } ?>
							<?php echo get_post_meta($loc_name->ID,'tbvets_city',true); ?>, <?php echo get_post_meta($loc_name->ID,'tbvets_state',true); ?>  <?php echo get_post_meta($loc_name->ID,'tbvets_zip',true); ?> <?php $check = get_post_meta($loc_name->ID,'tbvets_map',$true); if($check): ?><span class="map-link"><small><a href="<?php echo htmlentities(get_post_meta($loc_name->ID,'tbvets_map',true)); ?>">(View map)</a></small></span><?php endif; ?></p>

							<p class="hours"><strong>Hours:</strong><br />
							<?php echo nl2br(strip_tags(get_post_meta($loc_name->ID,'tbvets_hours',true))); ?></p>

					</div>
			<?php } else { ?>
			<h1>View Staff by Location:</h1>
			<?php
			$locs = get_posts(array('post_type' => 'location'));
			foreach($locs as $loc_name):
			?>
				<h2><a href="<?php self_link(); ?>?lid=<?php echo $loc_name->ID; ?>"><?php echo $loc_name->post_title; ?></a></h2>
				<p><a href="<?php self_link(); ?>?lid=<?php echo $loc_name->ID; ?>"><?php echo get_the_post_thumbnail($loc_name->ID, array(160,200), array('class'=>'frame')); ?></a>
				<br />
					<?php echo get_post_meta($loc_name->ID,'tbvets_address1',true); ?>, <?php echo get_post_meta($loc_name->ID,'tbvets_city',true); ?>
				</p>
			<?php endforeach; ?>
			<?php } ?>
		</section>

		<div class="content">

			<?php if(isset($loc_name)) { ?>
			<h1>Staff</h1>
			<?php
			if(current_user_can('manage_options')) {
				global $wp_query;
				//print_r($wp_query->query_vars);
			}
			$staff_ids = rpt_get_object_relation($loc_name->ID, array('staff'));
			query_posts(array('post_type' => 'staff','post__in' => $staff_ids,'posts_per_page'=>10, 'orderby' => 'menu_order','order'=>'ASC','paged'=>$paged));
			global $wp_query;
			if($wp_query->found_posts > 10):
				$big = intval($wp_query->found_posts.'000');
						echo paginate_links( array(
						'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $wp_query->max_num_pages
						) );
					endif;
			while(have_posts()) : the_post();
			?>
				<?php global $post; ?>
				<div id="post-<?php the_ID(); ?>" >
					<div class="entry-content">
						<div class="staff-box">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(115,115),array('class'=>'post-image bio-image frame')); ?></a>
							<div class="staff-details">
								<?php if(get_post_meta($post->ID,'tbvets_staff_years')) { ?>
								<span class="tiny"><strong>Employee Since: </strong><?php echo date("Y" , strtotime(get_post_meta( $post->ID, 'tbvets_staff_years', true) ) ); ?>		</span><br/>
								<?php } ?>
								<span class="tiny"><strong>Find me at: </strong>
								<?php $locations = get_related_item($post->ID, 'location', array('post_title','ID','guid'), false); $i = 0;
								foreach($locations as $location) :
									$pre = ($i == 0) ? ' ' : ', ';
									echo $pre. '<a href="'.$location->guid .'">'.$location->post_title .'</a>';
									$i++;
								endforeach;
								?>
							</div>
						</div>

						<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
						<?=string_limit_words(get_the_content(),50); ?>...
					</div><!-- Entry Content -->
				</div><!-- #post-## -->
			<div class="clear"></div>
		<?php endwhile; ?>

		<?php } else { ?>

		 <h1>About Us</h1>
		 <?php global $post; ?>
		 <?php $staff = new WP_Query("page_id=" . $post->ID); ?>
			<?php while($staff->have_posts()) : $staff->the_post(); ?>
			<?php the_content(); ?>
		 	<?php endwhile; ?>

		 <?php } ?>


		</div><!-- #content -->

	<?php get_footer(); ?>
<?php } ?>
