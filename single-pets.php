<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

		<div id="main" class="content-leads">
			<div class="content">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1><?php the_title(); ?></h1>
                    	<div class="adoption-details">
                    	<?php global $post; ?>
						<?php the_date(); ?> / <?php location('post_title',get_post_meta($post->ID,'related_location',true)); ?>
					</div><!-- .entry-meta -->

					<div class="entry-content">
						<?php the_post_thumbnail('medium',array('class' => 'alignright frame')); ?>
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
					</div><!-- .entry-content -->

<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their description, show a bio on their entries  ?>
					<div id="entry-author-info">
						<div id="author-avatar">
							<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentyten_author_bio_avatar_size', 60 ) ); ?>
						</div><!-- #author-avatar -->
						<div id="author-description">
							<h2><?php printf( esc_attr__( 'About %s', 'twentyten' ), get_the_author() ); ?></h2>
							<?php the_author_meta( 'description' ); ?>
							<div id="author-link">
								<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
									<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'twentyten' ), get_the_author() ); ?>
								</a>
							</div><!-- #author-link	-->
						</div><!-- #author-description -->
					</div><!-- #entry-author-info -->
<?php endif; ?>

					<div class="entry-meta">
						<?php twentyten_posted_on(); ?>
					</div><!-- .entry-meta -->

					<div class="entry-utility">
						<?php twentyten_posted_in(); ?>
						<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-utility -->
				</div><!-- #post-## -->

				<div id="nav-below" class="navigation">
					<div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'twentyten' ) . '</span> %title' ); ?></div>
					<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'twentyten' ) . '</span>' ); ?></div>
				</div><!-- #nav-below -->

				<?php comments_template( '', true ); ?>

<?php endwhile; // end of the loop. ?>

			</div><!-- #content -->
		
		<section class="supporting">
        	<p><a href="http://feeds.feedburner.com/TampaAnimalAndBirdHospitals" rel="alternate" type="application/rss+xml"><img src="http://www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt="" style="vertical-align:middle;border:0"/></a>&nbsp;<a href="http://feeds.feedburner.com/TampaAnimalAndBirdHospitals" rel="alternate" type="application/rss+xml">Subscribe in a reader</a></p>
            <form style="border:1px solid #ccc;padding:3px;text-align:center;" action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=TampaAnimalAndBirdHospitals', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true"><p>Subscribe by email:<br /><input type="text" style="width:140px" name="email"/></p><input type="hidden" value="TampaAnimalAndBirdHospitals" name="uri"/><input type="hidden" name="loc" value="en_US"/><input type="submit" value="Go!" /><br /><small>Delivered by <a href="http://feedburner.google.com" target="_blank">FeedBurner</a></small></form>
	<h2 style="margin-top:20px;">Adoption Links</h2>
			<ul><li><a href="http://www.adoptapet.com/" target="_blank">Adopt A Pet</a></li>
<li><a href="https://www.petfinder.com/" target="_blank">Pet Finder</a></li>
<li><a href="http://members.petfinder.com/~FL487/moreinfo.html" target="_blank">Georgeanns Homeless Hounds</a></li>
<li><a href="https://sites.google.com/site/lostangelsanimalrescuepages/" target="_blank">Lost Angels Animal Rescue</a><br /><a href="https://www.facebook.com/LostAngelsAnimalRescueInc" target="_blank">Lost Angels Animal Rescue on Facebook</a></li>
<li><a href="https://www.hillsboroughcounty.org/en/residents/animals-and-pets/pet-adoption/adopt-a-dog-or-cat" target="_blank">Hillsborough County Pet Adoptions</a></li>
<li><a href="http://suncoastanimalleague.org/" target="_blank">Suncoast Animal League</a></li>
<li><a href="http://www.labradorrescue.net/community/events/tpa" target="_blank">Labrador Retriever Rescue of Florida</a></li>
<li><a href="http://www.rugazrescue.com/" target="_blank">Rugaz Rescue</a></li>
<li><a href="http://www.earsrescue.org" target="_blank">E.A.R.S. Animal Rescue and Sanctuary</a></li>
<li><a href="http://tbhrr.org/" target="_blank">Tampa Bay House Rabbit Rescue</a></li>
<li><a href="http://floridaparrotrescue.com/" target="_blank">Florida Parrot Rescue</a></li></ul>

			<h2>Other Blog Posts</h2>
			<h3>View by Category</h3>
			<ul>
				<?php wp_list_categories( array('hierarchical' => '0', 'title_li' => '') ); ?> 
			</ul>
			
			<hr />
			
			<h3>View by Keywords</h3>
			<?php wp_tag_cloud( $args ); ?>
			
			<hr />
			
			<h3>View the Archives</h3>
			<ul>
				<?php wp_get_archives( 'type=monthly' ); ?>
			</ul>
         <!-- Facebook Badge START --><a href="http://www.facebook.com/AnimalHospitalsTampa" target="_TOP" title="Animal and Bird Hospitals of Tampa">Let's Be Friends!</a><br/><a href="http://www.facebook.com/AnimalHospitalsTampa" target="_TOP" title="Animal and Bird Hospitals of Tampa"><img src="http://www.tampabayvets.net/wp-content/uploads/2016/09/like-us-on-facebook.png" style="border: 0px;" /></a><!-- Facebook Badge END -->
		</section><!-- #sidebar -->

	<?php get_footer(); ?>