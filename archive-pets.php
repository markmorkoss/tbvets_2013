<?php
/**
Pets Archive
****/


get_header();
if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
else { $paged = 1; }
?>

	<div id="main" class="content-leads">
		<div class="content">
			<h1>Pets in need of a home</h1>
			<!-- <?php //var_dump ( get_page_number() ); ?> -->
			
			<?php 
			 query_posts("post_type=pets&showposts=8&$meta&paged=$paged");
			if(have_posts()) : while(have_posts()) : the_post();
			?>
                <div class="pet-post">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(200,150), array('class'=>'frame alignright block')); ?></a>
	
					<h2>
					<div class="entry-meta">
					<?php the_time('F d, Y'); ?> / <?php echo location('post_title',get_post_meta($post->ID,'related_location',true)); ?>
					</div><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					<div class="entry-content">
					<?php the_excerpt(); ?>
					</div>
	                <div style="clear:both;"></div>
                </div>
                
			<?php endwhile; ?>
				<div class="navigation">
				<div class="alignleft"><?php previous_posts_link('&laquo; Previous') ?></div>
				<div class="alignright"><?php next_posts_link('More &raquo;') ?></div>
				</div>
			<?php endif; wp_reset_query(); ?>
			
			
		</div><!-- #content -->

		<section class="supporting">
        	<p><a href="http://feeds.feedburner.com/TampaAnimalAndBirdHospitals" rel="alternate" type="application/rss+xml"><img src="http://www.feedburner.com/fb/images/pub/feed-icon16x16.png" alt="" style="vertical-align:middle;border:0"/></a>&nbsp;<a href="http://feeds.feedburner.com/TampaAnimalAndBirdHospitals" rel="alternate" type="application/rss+xml">Subscribe in a reader</a></p>
            <form style="border:1px solid #ccc;padding:3px;text-align:center;" action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=TampaAnimalAndBirdHospitals', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true"><p>Subscribe by email:<br /><input type="text" style="width:140px" name="email"/><input type="hidden" value="TampaAnimalAndBirdHospitals" name="uri"/><input type="hidden" name="loc" value="en_US"/><input type="submit" value="Go!" /><br /><small>Delivered by <a href="http://feedburner.google.com" target="_blank">FeedBurner</a></small></p></form>

			<h2 style="margin-top:20px;">Adoption Links</h2>
			<ul><li><a href="http://www.adoptapet.com/" target="_blank">Adopt A Pet</a></li>
<li><a href="https://www.petfinder.com/" target="_blank">Pet Finder</a></li>
<li><a href="http://members.petfinder.com/~FL487/moreinfo.html" target="_blank">Georgeanns Homeless Hounds</a></li>
<li><a href="https://sites.google.com/site/lostangelsanimalrescuepages/" target="_blank">Lost Angels Animal Rescue</a><br /><a href="https://www.facebook.com/LostAngelsAnimalRescueInc" target="_blank">Lost Angels Animal Rescue on Facebook</a></li>
<li><a href="https://www.hillsboroughcounty.org/en/residents/animals-and-pets/pet-adoption/adopt-a-dog-or-cat" target="_blank">Hillsborough County Pet Adoptions</a></li>
<li><a href="http://suncoastanimalleague.org/" target="_blank">Suncoast Animal League</a></li>
<li><a href="http://www.labradorrescue.net/community/events/tpa" target="_blank">Labrador Retriever Rescue of Florida</a></li>
<li><a href="http://www.rugazrescue.com/" target="_blank">Rugaz Rescue</a></li>
<li><a href="http://www.earsrescue.org" target="_blank">E.A.R.S. Animal Rescue and Sanctuary</a></li>
<li><a href="http://tbhrr.org/" target="_blank">Tampa Bay House Rabbit Rescue</a></li>
<li><a href="http://floridaparrotrescue.com/" target="_blank">Florida Parrot Rescue</a></li>
</ul>
			<h2>Want to Donate?</h2>
<ul><li><a href="https://veterinarycarefoundation.org/donate-today/" target="_blank">Veterinary Care Foundation</a></li>
<li><a href="https://givingandalumni.vetmed.ufl.edu/ways-to-give/" target="_blank">UF College of Veterinary Medicine</a></li></ul>
			<h2>View by Category</h2>
			<ul>
				<?php wp_list_categories( array('hierarchical' => '0', 'title_li' => '') ); ?> 
			</ul>
			
			<hr />
			
			<h2>View by Keywords</h2>
			<?php wp_tag_cloud( $args ); ?>
			
			<hr />
			
			<h2>View the Archives</h2>
			<ul>
				<?php wp_get_archives( 'type=monthly' ); ?>
			</ul>
            
             <!-- Facebook Badge START --><a href="http://www.facebook.com/AnimalHospitalsTampa" target="_TOP" title="Animal and Bird Hospitals of Tampa">Let's Be Friends!</a><br/><a href="http://www.facebook.com/AnimalHospitalsTampa" target="_TOP" title="Animal and Bird Hospitals of Tampa"><img src="http://www.tampabayvets.net/wp-content/uploads/2016/09/like-us-on-facebook.png" style="border: 0px;" /></a><!-- Facebook Badge END -->
           
           <div id='networkedblogs_nwidget_container' style='height:360px;padding-top:10px;'><div id='networkedblogs_nwidget_above'></div><div id='networkedblogs_nwidget_widget' style="border:1px solid #D1D7DF;background-color:#F5F6F9;margin:0px auto;"><div id="networkedblogs_nwidget_logo" style="padding:1px;margin:0px;background-color:#edeff4;text-align:center;height:21px;"><a href="http://networkedblogs.com/" target="_blank" title="NetworkedBlogs"><img style="border: none;" src="http://www.tampabayvets.net/wp-content/uploads/2016/09/blogs_logo_small.png" title="NetworkedBlogs"/></a></div><div id="networkedblogs_nwidget_body" style="text-align: center;"></div><div id="networkedblogs_nwidget_follow" style="padding:5px;"><a style="display:block;line-height:100%;width:90px;margin:0px auto;padding:4px 8px;text-align:center;background-color:#3b5998;border:1px solid #D9DFEA;border-bottom-color:#0e1f5b;border-right-color:#0e1f5b;color:#FFFFFF;font-family:'lucida grande',tahoma,verdana,arial,sans-serif;font-size:11px;text-decoration:none;" href="http://networkedblogs.com/blog/tampa_bay_vets_590940/?ahash=05faf381096fa1f1006eb66768cab2e8">Follow this blog</a></div></div><div id='networkedblogs_nwidget_below'></div></div><script type="text/javascript"><!--
if(typeof(networkedblogs)=="undefined"){networkedblogs = {};networkedblogs.blogId=590940;networkedblogs.shortName="tampa_bay_vets_590940";}
--></script><script src="http://nwidget.networkedblogs.com/getnetworkwidget?bid=590940" type="text/javascript"></script>						
           					
		</section><!-- #sidebar -->

<?php get_footer(); ?>