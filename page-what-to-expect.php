<?php
/**
Template Name: What to expect
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

	<div id="main" class="content-leads">
		<div class="content">
		<?php 
			global $post;
			query_posts('page_id='.$post->ID);
			while (have_posts()) : the_post(); 
			?>
			
			<h1><?php the_title(); ?></h1>
			<?php the_post_thumbnail('medium',array('class'=>'alignright frame')); ?>
			<?php the_content(); ?>
			
			<?php endwhile; ?>
		</div>

		<section class="supporting">
			<h2>Forms you may need</h2>
			<p>To ensure we are as thorough as possible with your pet&#8217;s annual and semi annual health visits, please visit the form page by click the link below, print out and complete the applicable form(s) and bring them to your pet&#8217;s appointment. Early detection is key for a happy, healthy pet, so be sure to let us know of any concerns you may have about your companion.</p>
			<ul>
			<?php 

if(is_page('what-to-expect')) {
					wp_nav_menu(array('menu'=>'What To Expect Form Page Button'));
				} 
if(is_page('cat-doctors')) {
					wp_nav_menu(array('menu'=>'Cat Doctors Form Page Button'));
				} 
if(is_page('north-bay')) {
					wp_nav_menu(array('menu'=>'North Bay Form Page Button'));
				} 
if(is_page('temple-terrace')) {
					wp_nav_menu(array('menu'=>'Temple Terrace Form Page Button'));
				} 
if(is_page('pebble-creek')) {
					wp_nav_menu(array('menu'=>'Pebble Creek Form Page Button'));
				} 
if(is_page('all-creatures-animal-hospital')) {
					wp_nav_menu(array('menu'=>'All Creatures Form Page Button'));
				} 
if(is_page('cheval')) {
					wp_nav_menu(array('menu'=>'Cheval Form Page Button'));
				} 
			?>
			</ul>
           <h2>Need Help Paying For Your Pet’s Medical Bills?</h2><p>As pet owners, we want to provide the best healthcare for our beloved pets. But sometimes the associated costs for these services can be over-whelming, especially in times of financial distress. We can help. We offer solutions that work for you, and your pet. <a href="/paying-pets-medical-bills/">Click here</a> for details.</p>
            <h2>Need Pet Insurance?</h2>
            <a href="https://trupanion.com/" target="_blank"><img src="http://www.tampabayvets.net/wp-content/uploads/2010/11/trupanion.png" width="240" /></a>
            <p>Visit <a href="http://www.trupanionpetinsurance.com/" target="_blank">Trupanion.com</a> for a free quote.  Complete, affordable accident and illness coverage.</p>
		</section>

<?php get_footer(); ?>