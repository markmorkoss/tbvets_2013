<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

<div id="main" role="main">
    <div class="welcome-section flex two-column">
        <div class="flex-column">
            <h1 class="m-b-35">Welcome To Tapma Bay Animal Hospital Group</h1>
            <p class="l-h-30 m-10">We’re proud to welcome you to our family of veterinary hospitals! <br><strong>We work every day in six locations in tampa.</strong></p>
            <div class="flex align-center button-wrap">
                <a class="button-blue">Book a clinic visit</a>
                <a class="locations" href="<?php echo get_permalink( get_page_by_path( 'hours-and-locations' ) )?>">Hours & Locations</a>
            </div>
        </div>
        <div class="flex-column">
            <p class="l-h-30 m-10">Our six full-service animal hospitals offer a range of services to take the very best care of your pet.</p>
            <?php include('partials/searchform.php'); ?>
        </div>
    </div>

    <div class="flex services-section">
        <div class="services-dog flex justify-center">
            <a href="<?php echo get_permalink( get_page_by_path( 'pet-services' ) )?>" class="button-navy">Our Services</a>
        </div>
        <div class="slider flex">
            <?php get_template_part('partials/slider','multiple',['post_type'=>'services']); ?>
        </div>
    </div>


    <div class="description-section flex two-column">
        <div>
            <h1 class="circle-before">We’re proud to welcome you to our family of veterinary hospitals!</h1>
        </div>
        <div class="justify-between flex-column">
            <div>
                <p>
                    The Tampa Bay Animal Hospital Group is locally owned, by Dr. Link Welborn and Dr. Timothy Lassett, who both continue to practice veterinary medicine in our hospitals.
                    <br><br>
                    Our hospitals are locally managed by our on-site practice managers and medical directors, all of whom have chosen to dedicate their careers to veterinary medicine!
                </p>
            </div>
            <a class="button-blue">About Us</a>
        </div>
    </div>


    <div class="video-section">
        <?php
        $features = get_posts(
            array(
                'post_type' => 'features',
                'numberposts' => '1'
            )
        );
        foreach( $features as $feat ) {
            $feature_video = get_post_meta($feat->ID, 'carousel_video_embed', true);
        }
        ?>
        <div class="feature feature-video">
            <?php echo $feature_video; ?>
        </div>
    </div>

    <div class="flex justify-around three-columns links-section">
        <a class="flex link-wrapper">
            <img class="m-24" src="<?php echo get_stylesheet_directory_uri(); ?>/img/cat-and-dog.svg"/>
            <div class="arrow-link-home m-24" href="<?php echo get_permalink( get_page_by_path( 'hours-and-locations' ) )?>">
                <p>Request an Appointment</p>
            </div>
        </a>
        <a class="flex link-wrapper">
            <img class="m-24" src="<?php echo get_stylesheet_directory_uri(); ?>/img/adoption.svg"/>
            <div class="arrow-link-home m-24" href="<?php echo get_permalink( get_page_by_path( 'pet' ) )?>">
                <p>Pet Adoption</p>
            </div>
        </a>
        <a class="flex link-wrapper">
            <img class="m-24" src="<?php echo get_stylesheet_directory_uri(); ?>/img/taxi.svg"/>
            <div class="arrow-link-home m-24" href="<?php echo get_post_permalink( 187 )?>">
                <p>Pet Taxi</p>
            </div>
        </a>
    </div>

    <div class="events-n-news-section">
        <div class="slider flex">
            <?php get_template_part('partials/slider','multiple',['post_type'=>'post', 'title'=>'Events & News']); ?>
        </div>
    </div>

<!--	<div id="cta">-->
<!--		<h2 id="callout">-->
<!--			<strong>Outstanding care</strong> for every client &amp; pet <em>every day</em> <span>with six locations in Tampa</span>-->
<!--		</h2>-->
<!--		<ul>-->
<!--			--><?php
//				$new = new WP_query('post_type=location&orderby=title&order=DESC');
//				while($new->have_posts()) : $new->the_post();
//			?>
<!--			<li>-->
<!--				<a href="--><?php //the_permalink(); ?><!--">-->
<!--					--><?php //the_title(); ?>
<!--				</a>-->
<!--				<span class="phone-no">-->
<!--					<a href="tel:--><?php //echo get_post_meta($post->ID,'tbvets_phone',true); ?><!--">-->
<!--						--><?php //echo get_post_meta($post->ID,'tbvets_phone',true); ?>
<!--					</a>-->
<!--				</span>-->
<!--			</li>-->
<!--			--><?php //endwhile; ?>
<!--		</ul>-->
<!--	</div> -->
<!--	<div class="carousel fouc-fix">-->
<!--		<div class="slidewrap" data-autorotate="4000">-->
<!--			<ul class="slider">-->
<!--				--><?php
//		   			$features = get_posts(
//		   				array(
//		   					'post_type' => 'features',
//		   					'numberposts' => '1'
//	   					)
//	   				);
//					foreach( $features as $feat ) :
//						$feature_link = get_post_meta( $feat->ID, 'tbvets_feature_link', true );
//						$feature_video = get_post_meta( $feat->ID, 'carousel_video_embed', true );
//		   			?>
<!--					<li class="slide">-->
<!--						<div class="feature-info" style="float:none;width:100%;margin-bottom:10px;margin-top:0;display:none">-->
<!--							 <h3><a href="--><?php //// echo $feature_link; ?><!--">--><?php ////echo $feat->post_title; ?><!--</a></h3> -->
<!--							--><?php //echo word_limit($feat->post_content, 300); ?>
<!--						</div>feature-info -->
<!--					--><?php //if( $feature_video ) : ?>
<!--						<div class="feature feature-video" style="width:100%;height:375px;">-->
<!--							--><?php //echo $feature_video; ?>
<!--						</div>-->
<!--					--><?php //else : ?>
<!--						<div class="feature feature-image">-->
<!--							<a href="--><?php //echo $feature_link; ?><!--" title="--><?php //echo $feature_link; ?><!--">-->
<!--								--><?php //echo get_the_post_thumbnail (
//									$feat->ID, 'large',
//									array( 'class' => 'feature-thumb wp-post-image', 'alt' 	=> $feature_link )
//								); ?>
<!--							</a>-->
<!--						</div>-->
<!--					--><?php //endif; ?>
<!---->
<!--					</li>-->
<!--				--><?php //endforeach; ?>
<!--			</ul>-->
<!--		</div>-->
<!--	</div>-->

<!---->
<!--	<div id="home-content">-->
<!--		<h2 id="callout" class="lowercallout" style="margin-bottom:0em; font-size:2em">-->
<!--			Welcome to <strong style="display:inline">Tampa Bay Animal Hospital Group!</strong>-->
<!--		</h2>-->
<!--		<div class="homecopy"><a href="https://www.tampabayvets.net/curbside-service-telehealth-mobile-services-and-more/" title="Read important information on our practice during COVID 19"><img src="https://www.tampabayvets.net/wp-content/uploads/2020/09/tbv-covid-banner.png"></a><br />-->
<!--			<p style="text-align:left; padding:0 15px;">-->
<!--				We’re proud to welcome you to our family of veterinary hospitals! The Tampa Bay Animal Hospital Group is locally owned, by <a href="/staff/dr-link-v-welborn-dvm-abvp/">Dr. Link Welborn</a> and <a href="/staff/dr-timothy-lassett-dvm-abvp-canine-and-feline/">Dr. Timothy Lassett</a>, who both continue to practice veterinary medicine in our hospitals. Our hospitals are locally managed by our on-site practice managers and medical directors, all of whom have chosen to dedicate their careers to veterinary medicine!-->
<!--			</p>-->
<!--			<p style="text-align:left; padding:0 15px;">-->
<!--				Our six full-service animal hospitals offer a <strong><a href="/pet-services/">range of services</a></strong> to take the very best care of your pet. Our professional and courteous team of veterinarians and highly-trained and caring staff are dedicated to providing the best possible medical, surgical and dental care for our highly-valued patients. Our veterinary hospitals maintain state-of-the-art technology and adhere to the <a href="https://www.aaha.org/" target="_blank">American Animal Hospital (AAHA)</a> Standards of Care, to ensure that our veterinarians and technicians can provide the absolute best standard of care for your pet!-->
<!--				</p>-->
<!--<p style="text-align:left; padding:0 15px;">-->
<!--				<strong><a href="/hours-and-locations/">Are you looking for a veterinarian near you?</a></strong> With six convenient locations throughout the Tampa area, The Tampa Bay Animal Hospital Group is your comprehensive solution for all pet care, boarding, and emergency veterinary needs. -->
<!--			</p>-->
<!--			<p style="text-align:left; padding:0 15px;">-->
<!--				For nearly 60 years, it’s been our goal to provide individualized patient care, while providing an exceptional experience for our clients. <strong><a href="/hours-and-locations/">Come visit our animal clinics today!</a></strong> Whether you’re from Westchase, Temple Terrace, Wesley Chapel, South Tampa, Lutz, Cheval, or even out-of-state, we’re proud to serve you! -->
<!--			</p>-->
<!--		</div>-->
<!--		<div class="home-widget">-->
<!--			<div class="widget-pad">-->
<!--				<h2 class="widget-header black"><a href="/hours-and-locations">Request an Appointment</a></h2>-->
<!--				<p><a class="teaser appt" href="/hours-and-locations">Start</a></p>-->
<!--				<p><strong>We&#8217;re open</strong> evenings &amp; weekends with 6 locations throughout Tampa.</p>-->
<!--			</div>-->
<!--		</div>-->
<!---->
<!--		<div class="home-widget adopt-a-pet">-->
<!--			<div class="widget-pad">-->
<!--			<h2 class="widget-header black"><a href="/pets">Pet Adoption</a></h2>-->
<!--				<p><a class="teaser adopt" href="/pets">Adopt a Pet</a></p>-->
<!--				<p><strong>Find a pet to bring home.</strong> Adopt from one of our locations.</p>-->
<!--			</div>-->
<!--		</div>-->
<!---->
<!--		<div class="home-widget last">-->
<!--			<div class="widget-pad">-->
<!--				<h2 class="widget-header black"><a href="/pet-services">Pet Taxi</a></h2>-->
<!--				<p><a class="teaser taxi" href="/services/pet-care-services/house-calls-mobile-services/">Get a pet taxi</a></p>-->
<!--				<p><strong>Need transportation?</strong> We&#8217;ll come pick up your pet for their next visit.</p>-->
<!--				<div id="tag-cloud">-->
<!--					<center>--><?php //wp_tag_cloud(array('smallest'=> 10,'largest'=> 10,'taxonomy' => 'service','orderby'=>'RAND')); ?><!--</center>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!---->
<!--	</div>-->
<!---->
<!--	<br style="clear: both;" />-->

<!--	<div id="news">-->
<!--		<section>-->
<!--			<h2>Community &amp; Events</h2>-->
<!--			--><?php
//			$today = current_time( 'mysql' );
//
//			$event_args = array(
//				'post_type'      => 'events',
//				'posts_per_page' => 1,
//				'meta_query'     => array( // phpcs:ignore
//					array(
//						'key'     => 'simplr_end_date',
//						'value'   => $today,
//						'compare' => '>=',
//						'order_by' => 'key',
//						'order' => 'ASC',
//					),
//				),
//				'meta_key'       => 'simplr_end_date', // phpcs:ignore
//				'order_by'       => 'meta_value',
//				'order'           => 'ASC',
//			);
//
//			$post_args = array(
//				'cat'            => 300,
//				'posts_per_page' => 1,
//			);
//
//			$event_query = new WP_Query( $event_args );
//			$com         = new WP_Query( $post_args );
//
//			if ( $event_query->have_posts() ) :
//				while ( $event_query->have_posts() ) :
//					$event_query->the_post();
//					// $end_date = get_post_meta( $post->ID, 'simplr_end_date' );
//					// echo '<pre style="visibility:hidden;">' . var_dump( $end_date ) . '</pre>';
//
//					?>
<!--					<a href="--><?php //the_permalink(); ?><!--">-->
<!--						--><?php
//						the_post_thumbnail( 'thumbnail', [
//							'class' => 'frame wp-post-image',
//							'alt'   => get_the_title(),
//							'title' => get_the_title(),
//						]);
//						?>
<!--					</a>-->
<!--					<h3>-->
<!--						<a href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a>-->
<!--					</h3>-->
<!--					<div class="news-content">-->
<!--						<p>--><?php //the_excerpt(); ?><!--</p>-->
<!--					</div>-->
<!--					--><?php
//				endwhile;
//			else :
//				while ( $com->have_posts() ) :
//					$com->the_post();
//					?>
<!--				<a href="--><?php //the_permalink(); ?><!--">-->
<!--					--><?php //the_post_thumbnail('thumbnail', ['class'=>'frame wp-post-image', 'alt'=>get_the_title(), 'title'=>get_the_title()]); ?>
<!--				</a>-->
<!--				<h3>-->
<!--					<a href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a>-->
<!--				</h3>-->
<!--				<div class="news-content">-->
<!--					<p>--><?php //the_excerpt(); ?><!--</p>-->
<!--				</div>-->
<!--			--><?php
//				endwhile;
//			endif;
//			wp_reset_postdata();
//			?>
<!--		</section>-->
<!--		<section>-->
<!--			<h2>Latest News</h2>-->
<!--			--><?php
//			$com = new WP_Query('post_type=post&showposts=1&category_name=News&showposts=1');
//			if ($com->have_posts()):
//				while($com->have_posts()) : $com->the_post(); ?>
<!--					<a href="--><?php //the_permalink(); ?><!--">-->
<!--						--><?php //the_post_thumbnail(array(150, 150), array('class'=>'frame wp-post-image', 'alt'=>get_the_title(), 'title'=>get_the_title())); ?>
<!--					</a>-->
<!--					<h3>-->
<!--						<a href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a>-->
<!--					</h3>-->
<!--					<div class="news-content">-->
<!--						<p>--><?php //the_excerpt(); ?><!--</p>-->
<!--					</div>-->
<!--			--><?php
//				endwhile;
//			endif;
//			wp_reset_postdata();
//			?>
<!--		</section>-->
<!--	</div> end news -->
	<!-- <div class="instagrams">
		<h2>Tampa Bay Animal Hospitals on <span>Instagram</span></h2>
		<section>
			<?php
				/*
				echo do_shortcode("[mgl_instagram_tag number=2 tag=NBPets]");
				echo do_shortcode("[mgl_instagram_tag number=2 tag=PCPets]");
				echo do_shortcode("[mgl_instagram_tag number=2 tag=CDpets]");
				echo do_shortcode("[mgl_instagram_tag number=2 tag=TTpets]");
				echo do_shortcode("[mgl_instagram_tag number=8 tag=TBVpets]");
				*/
			?>
		</section>
	</div>  -->
</div>
<?php get_footer(); ?>
