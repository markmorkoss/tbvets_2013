<?php
/**
Template Name: Make an Appointment
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

<div id="main" class="content-leads">
	<div class="content">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	
		<? if(isset($_POST['is_submit_3'])) { // if sent ?>
	
			<h1>Request sent</h1>
			<h2>Thank for your appointment request for <?php echo $_POST['input_8']; ?> Animal Hospital.<br />
			We&#8217;ll contact you shortly to confirm.  Please be aware that this is a request and we will be confirm your appointment over the phone.</h2>
			
			<? gravity_form(3); ?>
	
		<?php } elseif(!isset($_GET['loc'])) { // if blank slate ?>
	
			<h1>Request an Appointment</h1>
			<br />
			<div id="form-step-1">
				<?php
				$locs = new WP_query("post_type=location");
				while($locs->have_posts()) : $locs->the_post();
					?>
					<div class="location-box">
						<a href="?loc=<?=$post->ID; ?>"><?php the_post_thumbnail(array(140,140), array('class'=>'alignright frame')); ?></a>
						<h3><a href="?loc=<?=$post->ID; ?>"><?=$post->post_title; ?></a></h3>
						<p class="address"><?php echo get_post_meta($post->ID,'tbvets_address1',true); ?>
								<?php if((get_post_meta($post->ID,'tbvets_address2',true))) { ?><?php echo get_post_meta($post->ID,'tbvets_address2',true); ?><?php } ?>
								<?php echo get_post_meta($post->ID,'tbvets_city',true); ?>, <?php echo get_post_meta($post->ID,'tbvets_state',true); ?>  <?php echo get_post_meta($post->ID,'tbvets_zip',true); ?></p>
		
						<p><a href="?loc=<?=$post->ID; ?>" class="green-button tiny">Choose this location</a></p>
	
						<p><strong>Hours:</strong><br />
						<?php echo nl2br(strip_tags(get_post_meta($post->ID,'tbvets_hours',true))); ?></p>
		
				</div><!-- location-box -->
				<?php endwhile; ?>
			</div><!-- step-1 -->
	
		<?php } else { // if step 2 ?>
	
			<?php $new = get_post($_GET['loc']); ?>
			<h1>Request an Appointment for<br />
			<span><?=$new->post_title; ?></span></h1>
			<div id="form-step-2">
				<? gravity_form(3); ?>
			</div><!-- step-2 -->
	
		<?php } ?>
	
	<?php endwhile; ?>
	</div>

<?php get_footer(); ?>