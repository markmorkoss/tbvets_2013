<?php
/**
Template Name: LW Appointment
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage tbvets
 */
?>
<!DOCTYPE html><html <?php language_attributes(); ?>><head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title>Tampa Vet - <?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( 'Tampa Vet |', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
    <meta name="robots" content="index, follow" />
    <meta content="Tampa Vet with four  Tampa locations in Westchase / Town-n-Country, Temple Terrace, New Tampa, and South Tampa.  Full-service animal hospital.  High-quality, top-of-the-line
      diagnostics and medial facilities, caring and compassionate staff." name="description">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<script type="text/javascript" src="http://use.typekit.com/xlu2tzd.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
		<div id="headerchange">
		<div id="mainbar" class="appointment">
			<h1>Request an appointment</h1>
			<?php gravity_form(2, false, false); ?>
		</div>
		</div>
		
</body>
</html>				