<?php
/**
Template Name: Staff Page
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

	<div id="main" class="content-trails">

	<section class="supporting">
	<?php
	$loc = $_GET['lid'];
	if(isset($loc)) {
		$loc = intval($loc);
		$loc_name = &get_post($loc);
		 ?>
				<h1>Location:<br /><?php echo $loc_name->post_title; ?></h1>
				<p><a href="<?php bloginfo('url'); ?>/staff/" title="Back to full staff list">&larr; Back to all staff</a></p>

				<div class="intro">
						<a href="<?php bloginfo('url'); ?>/location/<?php echo $loc_name->post_name; ?>/" title="<?php echo $loc_name->post_title; ?>" >
							<?php echo get_the_post_thumbnail($loc_name->ID, array(160,200), array('class'=>'frame')); ?>
						</a>
						</div>

						<div class="info">
						<strong><a href="<?php bloginfo('url'); ?>/location/<?php echo $loc_name->post_name; ?>/"><?php echo $loc_name->post_title; ?></a> <span class="tiny">(<?php echo get_post_meta($loc_name->ID, 'tbvets_subtitle', true); ?>)</span></strong>
						<p class="address"><?php echo get_post_meta($loc_name->ID,'tbvets_address1',true); ?>
						<?php if((get_post_meta($loc_name->ID,'tbvets_address2',true))) { ?><?php echo get_post_meta($loc_name->ID,'tbvets_address2',true); ?><?php } ?>
						<?php echo get_post_meta($loc_name->ID,'tbvets_city',true); ?>, <?php echo get_post_meta($loc_name->ID,'tbvets_state',true); ?>  <?php echo get_post_meta($loc_name->ID,'tbvets_zip',true); ?> <?php $check = get_post_meta($loc_name->ID,'tbvets_map',$true); if($check): ?><span class="map-link"><small><a href="<?php echo htmlentities(get_post_meta($loc_name->ID,'tbvets_map',true)); ?>">(View map)</a></small></span><?php endif; ?></p>

						<p class="hours"><strong>Hours:</strong><br />
						<?php echo nl2br(strip_tags(get_post_meta($loc_name->ID,'tbvets_hours',true))); ?></p>

				</div>
		<?php } else { ?>
		<h1>View Staff by Location:</h1>
		<?php
		$locs = get_posts(array('post_type' => 'location'));
		foreach($locs as $loc_name):
			?>
			<h2><a href="<?php self_link(); ?>?lid=<?php echo $loc_name->ID; ?>"><?php echo $loc_name->post_title; ?></a></h2>
			<p><a href="<?php self_link(); ?>?lid=<?php echo $loc_name->ID; ?>"><?php echo get_the_post_thumbnail($loc_name->ID, array(160,200), array('class'=>'frame')); ?></a>
			<br />
				<?php echo get_post_meta($loc_name->ID,'tbvets_address1',true); ?>, <?php echo get_post_meta($loc_name->ID,'tbvets_city',true); ?>
			</p>
		<?php endforeach; ?>
		<?php } ?>
	</section>

	<div class="content">

		<?php if(isset($loc)) { ?>
		<h1>Team Members & Colleagues</h1>
		<?php
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$staff_ids = rpt_get_object_relation($loc, array('staff'));

		query_posts(array('post_type' => 'staff','post__in' => $staff_ids,'numberposts' => -1,'posts_per_page'=>100, 'orderby' => 'menu_order','order'=>'ASC'));

		while(have_posts()) : the_post();
		?>
			<?php global $post; ?>
			<div class="staff-box">
				<div class="staff-details">
					<a href="<?php the_permalink(); ?>" class="img-link"><?php the_post_thumbnail(array(115,115),array('class'=>'post-image bio-image frame')); ?></a>
					<p>
					<?php if(get_post_meta($post->ID,'tbvets_staff_years')) { ?>
					<span class="tiny">
						<strong>Employee Since: </strong><?php echo date("Y" , strtotime(get_post_meta( $post->ID, 'tbvets_staff_years', true) ) ); ?>
					</span><br/>
					<?php } ?>
					<span class="tiny"><strong>Find them at: </strong>
					<?php $locations = get_related_item($post->ID, 'location', array('post_title','ID','guid'), false); $i = 0;
					foreach($locations as $location) :
						$pre = ($i == 0) ? ' ' : ', ';
						echo $pre. '<a href="'.$location->guid .'">'.$location->post_title .'</a>';
						$i++;
					endforeach;
					?>
					</p>
				</div>
				<div class="staff-description">
					<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					<? the_excerpt(); ?>
				</div>
			</div>
	<?php endwhile; ?>

	<?php } else { ?>

	 <h1>About Us</h1>
	 <?php global $post; ?>
	 <?php $staff = new WP_Query("page_id=" . $post->ID); ?>
		<?php while($staff->have_posts()) : $staff->the_post(); ?>
		<?php the_content(); ?>
	 	<?php endwhile; ?>

	 <?php } ?>


	</div><!-- #content -->

<?php get_footer(); ?>
