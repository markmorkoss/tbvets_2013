<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

		
		<div id="main" class="content-leads">
			<div class="content">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1><?php the_title(); ?></h1>
                    
					<div class="entry-content">
						<?php the_post_thumbnail('medium',array('class' => 'alignright frame')); ?>
						<?php the_content(); ?>
						
					</div><!-- .entry-content -->

					
				</div><!-- #post-## -->


<?php endwhile; // end of the loop. ?>

			</div><!-- #content -->

		<?php get_sidebar(); ?>
	
<?php get_footer(); ?>