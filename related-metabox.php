<?php

add_action('add_meta_boxes', 'vets_add_meta_box');
add_action('save_post','save_vets_rel');

function vets_add_meta_box() {
	add_meta_box('vets_related', 'Related Location', 'vets_rel_metabox', 'pets', 'normal', 'high' );
}

function vets_rel_metabox($args) {
	?>
	<div id="metabox">
	<?php 
	$field = new Metabox();
	$old = get_post_meta($_GET['post'],'related_location',true);
	$options = array('test','testing');
	$args = (object) array(
		'key'=>'related_location', 
		'label'=>'', 
		'single'=>'true',
		'type'=>'related_post',
		'options' => array('post_type' => 'location')
	);
	$field->make_form_field($args, $old, $i = 0);
	?>	
	<?php wp_nonce_field(-1,'related_location_nonce'); ?>
	</div>
	<?php
}

function save_vets_rel($post_id) {
	if($_POST['post_type'] == 'pets' && isset($_POST['related_location'])) {
		if(!wp_verify_nonce($_POST['related_location_nonce'],-1)) {
			die("Your nonce did not verify");
		}
	}
	$old = get_post_meta($post_id,'related_location',true);
	$save = new Metabox();
	$save->save_meta_field($post_id,'related_location','related_post', $_POST['related_location'], $old);
}

?>