<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

		<div id="main" class="content-leads">
			
			<div class="content">

				<?php 
				global $wp;
				echo '<pre>';
				print_r($wp);
				echo '</pre>';
				$tax = $wp->query_vars['term']; 
				?>
				<?php
				$term = get_term_by('slug', $tax,'service') ;
				?>
				<h1><?php echo $term->name; ?></h1>
				<img src="<?php echo get_metadata('term', $term->term_id, 'service_image',true); ?>" alt="<?php echo $term->name; ?>" class="alignright frame">
				<p><?php echo $term->description; ?></p>
		
			</div><!-- #content -->
			
		<?php get_sidebar(); ?>
		
<?php get_footer(); ?>
