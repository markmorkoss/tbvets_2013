(function($) {
	$(document).ready(function() {
		$('.toggle').on('click', function () {
			$('.big-menu').addClass('open');
			$('#back').show();
			$('body').addClass('menu-open');
		});
		$('#back').on('click', function () {
			$('.big-menu').removeClass('open');
			$('#back').hide();
			$('body').removeClass('menu-open');
		});
		$('#slide-down').on('click',function () {
			$('html, body').animate({
				scrollTop: $(document).height()
			}, 1500);
			return false;
		});

		var footer = document.getElementsByTagName("footer")[0];
		var bottom = footer.offsetTop;
		var header = document.getElementById("header");
		var sticky = header.offsetTop;

		if(window.pageYOffset + $(window).height() > bottom ){
			$('#slide-down').css({'opacity':'0'});
		} else{
			$('#slide-down').css({'opacity':'1'});
		}
		window.addEventListener('scroll', arrowSlideDownCheck);
		window.addEventListener('scroll', stickyHeader);

		function arrowSlideDownCheck() {
			if(window.pageYOffset + $(window).height() > bottom ){
				$('#slide-down').css({'opacity':'0'});
			} else{
				$('#slide-down').css({'opacity':'1'});
			}
		}

		function stickyHeader(){
			if (window.pageYOffset > sticky) {
				header.classList.add("sticky");
				$('body').addClass('sticky-body');
			} else {
				header.classList.remove("sticky");
				$('body').removeClass('sticky-body');
			}
		}

		const slider = new Swiper('.slider-multiple', {
			slidesPerView: 4,
			grabCursor: true,
			loop: true,
			spaceBetween: 32,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		});
	});

})( jQuery );
