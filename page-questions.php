<?php
/**
Template Name: Questions Page	
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>
<style>

.recaptcha_input_area{
height: 30px!important;
}
#recaptcha_area, #recaptcha_table{
	height:127px !important;
	overflow:hidden !important;
}
</style>
	<div id="main" class="content-trails">
		<section class="supporting">
			<h1>Ask Our Veterinarians!</h1>
			<p>
				We all know that pet ownership isn’t always easy — but our team of veterinarians and veterinary experts are here to help! Have you noticed a behavior change in your animal recently? Or perhaps you’re not sure if you need to bring them in to our animal clinic, or just wait it out? 
				<br><br>
				You can send questions directly to our animal hospital and our team of veterinarians will promptly provide our insight!
			</p>

			<p><img src="/wp-content/uploads/2015/01/staff_2014.png" /></p>
			
			<h3>Ask the vets your question</h3>
			<?php 
			/*$captcha = set_session_captcha ();
			if(isset($_POST['submit'])) 
			{ 
				$data = array(
					'post_title' => $_POST['qtitle'], 
					'email' => $_POST['qemail'], 
					'name' => $_POST['qname'],
					'boogie' => $_POST['boogie']
				);
				add_question($data);			
			}*/
			?>
		<?php gravity_form(1, false, false, false, '', false); ?>
		<!-- <script type="text/javascript" src="https://form.jotform.com/jsform/81337623884161"></script> -->

		</section>
		

		<div class="content">
			<div id="filters" class="vet-question-search">
				<h2>Quick Search</h2>
				<div>
					<?php $link = get_term_link(38, 'qcat'); if ( $link && !is_a($link, 'WP_Error') ) : ?>
						<a id="dogs" class="main-categories" href="<?php echo str_replace( 'qcat', 'ask-a-vet', $link); ?>">Dogs</a>
					<?php endif; ?>
					<?php $link = get_term_link(56, 'qcat'); if ( $link && !is_a($link, 'WP_Error') ) : ?>
						<a id="cats" class="main-categories" href="<?php echo str_replace( 'qcat', 'ask-a-vet', $link); ?>">Cats</a>
					<?php endif; ?>
					<?php $link = get_term_link(116, 'qcat'); if ( $link && !is_a($link, 'WP_Error') ) : ?>
						<a id="reptiles" class="main-categories" href="<?php echo str_replace( 'qcat', 'ask-a-vet', $link); ?>">Reptiles</a>
					<?php endif; ?>
					<?php $link = get_term_link(93, 'qcat'); if ( $link && !is_a($link, 'WP_Error') ) : ?>
						<a id="birds" class="main-categories" href="<?php echo str_replace( 'qcat', 'ask-a-vet', $link); ?>">Birds</a>
				  	<?php endif; ?>
				  </div>
				<h3 style="clear:both;">Search Questions &amp; Categories</h3>
				<?php 
					$q = $qcat = ''; 
					if(isset($_GET['q_submit'])) { $q = $_GET['q_search']; }
					if(get_query_var('quecat')) { $qcat = get_query_var('quecat'); }
					$qcat = (strstr($qcat,'/'))?explode('/',$qcat):$qcat;
					if(is_array($qcat)) {
						$paged = ($qcat[1] != 'page')?$qcat[1]:$qcat[2];
						$qcat = ($qcat[0]!='page')?$qcat[0]:'';
					}
				?>
				<form id="q-search" action="<?php self_link(); ?>" method="get" id="searchform" class="askavet-form">
					<input type="text" name="q_search" value="Search questions..." onfocus="clearText(this)" />
					<input type="submit" class="green-button" value="Search" name="q_submit" />
				</form>

				<!--<h3>Browse Categories:</h3>
				<ul class="categories">
				<li><a href="<?php bloginfo('url'); ?>/ask-a-vet/" title="show all" >All</a></li>

					<?php $terms = get_terms('qcat', array('hide_empty' => false)); ?>
					<?php foreach($terms as $term) : ?>	
						<li><a href="<?php bloginfo('url'); ?>/ask-a-vet/<?php echo $term->slug; ?>/" title="filter for this term" ><?php echo $term->name; ?></a></li>
					<?php endforeach; ?>
				</ul> -->
			</div>
				
			<h2>Browse Questions:</h2>
			<?php 
			$paged = (isset($paged)) ? $paged : 1;
			query_posts("post_type=questions&s=$q&qcat=$qcat&paged=$paged&post_per_page=10");			
			if(have_posts()):
					global $wp_query;
					//do pagination
					if($wp_query->found_posts > 10) {
					$big = intval($wp_query->found_posts.'000');	
					echo paginate_links( array(
						'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $wp_query->max_num_pages
						) );
					}
				while(have_posts()) : the_post(); 
				?>
				<div class="question">			
					<h3>Q. <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
					<div class="question-answer"><?php the_excerpt(); ?></div>
				</div>
			<?php endwhile; endif; ?>
			
			<?php if (  $blog->max_num_pages > 1 ) : ?>
				<div id="nav-below" class="navigation">
					<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts') ); ?></div>
					<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>') ); ?></div>
				</div><!-- #nav-below -->
			<?php endif; ?>
				
		</div><!-- #content -->

<?php get_footer(); ?>