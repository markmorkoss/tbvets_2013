<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

	<div id="main" class="content-leads">
		<div class="content">

			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<?php $this_post_id = get_the_ID(); ?>
					<?php if(has_post_thumbnail($post->ID)) : ?>
						<?php echo get_the_post_thumbnail($post->ID,array(426,200,true),array('class'=>'frame')); ?>
					<?php endif; ?>

					<h1>Community &amp; Events:<br />
					<?php the_title(); ?></h1>
						<strong class="start-date"><?php start_date(); ?> - <?php end_date(); ?></strong>

					<?php the_content(); ?>

					<div class="entry-meta">
						<?php twentyten_posted_on(); ?>
					</div><!-- .entry-meta -->



			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->

		<section class="supporting">
			<h2>Other Upcoming Events</h2>
			<?php
				$today = date("Y-m-d");
				$args = array(
					'post_type' => 'events',
					'meta_query' => array(
						array(
							'key' => 'simplr_end_date',
							'value' => $today,
							'compare' => '>='
						)
						),
						'meta_key' => 'simplr_end_date',
						'order_by' => 'meta_value_num',
						'order' => 'ASC',
				);
				$query = new WP_Query($args);
				if ($query->have_posts()):
					while($query->have_posts()):
						$query->the_post();
						$title = get_the_title($post->ID);
						$link = get_permalink($post->ID);
						$start = get_post_meta(get_the_ID(),'simplr_start_date', true);
						$end = get_post_meta(get_the_ID(),'simplr_end_date', true);
						$startDate = date('F d, Y', strtotime($start));
						$endDate = date('F d, Y', strtotime($end));
						$meta = get_post_meta( $post->ID );
						echo '<div class="event" style="border-bottom:1px solid #999;">';

							// echo 'today'.$today.'<br>';
							// echo 'start '.$start.'<br>';
							// echo print_r($meta);

							// if(has_post_thumbnail($post->ID)) :
							//  	echo '<a href="'.$link.'">';
							//  		echo get_the_post_thumbnail($post->ID,array(245,250),array('class'=>'frame','title' => $title, 'alt' => $title ));
							// 	echo '</a>';
							// endif;
							echo '<div class="clear clearfix"></div>';
							echo '<span class="start-date" style="margin-top:20px;">';
							 	echo $startDate.' - '.$endDate;
							echo '</span>';
							echo '<h2 style="margin-top:0;">';
								echo '<a href="'. $link .'" title="'. $title .'">';
									echo $title;
								echo '</a>';
							echo '</h2>';
						echo '</div><!-- event -->';
					endwhile;
				endif;
				wp_reset_postdata();
			?>
		</section>

<?php get_footer(); ?>
