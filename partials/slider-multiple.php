<div class="swiper-container flex-column slider-multiple">
    <div class="swiper-buttons flex justify-between align-center m-b-65">
        <?php if($args['title']!=''){ ?>
        <h1><?php echo $args['title'];?></h1>
        <?php } ?>
        <div class="buttons flex">
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
    <div class="swiper-wrapper">
        <?php


        			$serv_query = new WP_Query('post_type='.$args['post_type'].'&showposts=10');

        			if ( $serv_query->have_posts() ) :
        				while ( $serv_query->have_posts() ) :
                            $serv_query->the_post();
        ?>
        <div class="swiper-slide flex-column">
            <a href="<?php echo get_post_permalink()?>" class="slide-link flex-column">
                <div class="image_container" style="background-image: url('<?php echo get_the_post_thumbnail_url();?>')"></div>

                <p class="m-10 w-500"><?php echo get_the_title() ?></p>
                <p class="m-10 w-300"><?php echo get_the_excerpt() ?></p>
            </a>
        </div>
        <?php endwhile;
        			endif;
        			wp_reset_postdata();?>
    </div>
</div>