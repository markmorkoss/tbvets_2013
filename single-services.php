<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

	<div id="main" class="content-leads">
	
		<div class="content">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<?php global $post; 
				$parent = $post->post_parent;
				?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1><?php the_title(); ?></h1>

					<?php if(has_post_thumbnail($service->ID)) : ?>
						<?php echo get_the_post_thumbnail($service->ID,array(250,250),array('class'=>'alignright service-thumb frame')); ?>
					<?php else: $icon = 'http://vets.bigseapreview.com/wp-content/uploads/2010/08/service-image.jpg'; ?>
						<img src="<?php echo $icon; ?>" class="alignright service-thumb frame" alt="<?php echo $service->post_title; ?>" />
					<?php endif; ?>
							
					<?php the_content(); ?>

				</div><!-- #post-## -->
			<?php endwhile; // end of the loop. ?>

			<h2>Our services are available from the following animal hospital locations:</h2>
			<div class="available-locations">
			<?php $locations = get_related_item($post->ID, 'location', array('post_title','ID','guid'), false); $i = 0;
				foreach($locations as $location) : 
			?>
				<?php $post = &get_post(intval($location->ID)); ?>
				
					<div class="location-box">
						<div class="intro">
						<a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'medium', array('class'=>'frame')); ?></a>
						</div>
						
						<div class="info">
						<h3 style="font-size:15px;"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> <br> <span class="tiny" style="margin-top:-10px;">(<?php echo get_post_meta($post->ID, 'tbvets_subtitle', true); ?>)</span></h3>
                        <a href="http://www.tampabayvets.net/hours-and-locations/" class="button" style="font-size:10px;">Make an Appointment</a>
						<p class="address-small"><?php echo get_post_meta($post->ID,'tbvets_address1',true); ?>
						<?php if((get_post_meta($post->ID,'tbvets_address2',true))) { ?><?php echo get_post_meta($post->ID,'tbvets_address2',true); ?><?php } ?><br>
						<?php echo get_post_meta($post->ID,'tbvets_city',true); ?>, <?php echo get_post_meta($post->ID,'tbvets_state',true); ?>  <?php echo get_post_meta($post->ID,'tbvets_zip',true); ?> <?php $check = get_post_meta($post->ID,'tbvets_map',$true); if($check): ?><br>
						<span class="map-link"><small><a href="<?php echo htmlentities(get_post_meta($post->ID,'tbvets_map',true)); ?>">(View map)</a></small></span><?php endif; ?>
					</p>
					
						</div>
					</div><!-- location -->

									
				<?php endforeach; ?>
			</div>
		</div><!-- #content -->

		<section class="supporting">
			<h2>Related Services</h2>
			<ul id="tag-cloud">
				<?php $terms = get_posts(array('post_type' => 'services', 'post_parent'=> $parent, 'numberposts' => '-1', 'orderby'=> 'title','order'=>'ASC')); ?>
					<?php foreach($terms as $term) : ?>
						<li><a href="<?php echo $term->guid; ?>" title="more about $term" ><?php echo $term->post_title; ?></a></li>
					<?php endforeach; ?>
			</ul>
			<div id="apptbar">
			<h1>Request an appointment</h1>
<script type="text/javascript" src="https://form.jotform.com/jsform/82886760384168"></script>	
			</div>	
		
		</section>

<?php get_footer(); ?>