<?php
/**
Template Name: Blog Page
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

	<div id="main" class="content-leads">
		<div class="content">
		<h1>News</h1>
			<?php 
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			query_posts("showposts=10&paged=$paged");
			if(have_posts()) : while(have_posts()) : the_post();
			?>
                
				<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(200,150), array('class'=>'frame alignright block')); ?></a>

				<h2>
                     <div class="entry-meta">
						<?php the_date(); ?>
					</div><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				<div class="entry-content">
				<?php the_excerpt(); ?>
				</div>
                <div style="clear:both;"></div>
                
			<?php endwhile; ?>
				<div class="navigation">
				<div class="alignleft"><?php previous_posts_link('&laquo; Previous') ?></div>
				<div class="alignright"><?php next_posts_link('More &raquo;') ?></div>
				</div>
			<?php endif; ?>
			
			
		</div><!-- #content -->

		<section class="supporting">
			
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog") ) : ?>
			<?php endif; ?>
			
			<hr/>
		</section><!-- #sidebar -->

<?php get_footer(); ?>