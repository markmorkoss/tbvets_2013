<?php
/**
Template Name: Locations & Hours
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

	<div id="main" class="content-trails">
		<section class="supporting">
			<h1>Request an appointment</h1>
<script type="text/javascript" src="https://form.jotform.com/jsform/82886760384168"></script>			
		</section>
		<div class="content">
			<h1>Hours &amp; Locations</h1>
			<?php 
			$locs = new WP_query( array ('post_type' => 'location', 'order' => 'ASC' ) );
			while($locs->have_posts()) : $locs->the_post();
				?>
				<div class="location-block">
						
					<?php the_post_thumbnail(array(202,138)); ?>
					
					<div class="address">
				
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> <small>(<?php echo get_post_meta($post->ID, 'tbvets_subtitle', true); ?>)</small></h3>
						<p><?php echo get_post_meta($post->ID,'tbvets_address1',true); ?><br/>
						<?php if((get_post_meta($post->ID,'tbvets_address2',true))) { ?><?php echo get_post_meta($post->ID,'tbvets_address2',true) . '<br/>'; ?><?php } ?>
						<?php echo get_post_meta($post->ID,'tbvets_city',true); ?>, <?php echo get_post_meta($post->ID,'tbvets_state',true); ?>  <?php echo get_post_meta($post->ID,'tbvets_zip',true); ?> <?php $check = get_post_meta($post->ID,'tbvets_map',$true); if($check): ?><span class="map-link"><small><a href="<?php echo htmlentities(get_post_meta($post->ID,'tbvets_map',true)); ?>">(View map)</a></small></span><?php endif; ?><br/>
						<?php echo get_post_meta($post->ID,'tbvets_phone',true); ?></p>
						
					</div><!-- end address -->
					
					<div class="hours">
					
						<h4>Hours:</h4>
						<?php $hours = explode('<br>', get_post_meta($post->ID,'tbvets_hours',true)); ?>
						<ul>
							<?php foreach ( $hours as $hour ) : ?>
							<li><?php echo $hour ?></li>
							<?php endforeach; ?>
						</ul>



					
					</div><!-- end hours -->
				
				</div><!-- end location-block -->
			<?php endwhile; ?>			

			<hr />

			<h2>Our network of veterinary clinics is proud to serve communities from across the Florida area.</h2>
			<p>
				If you’re from one of the following communities, you’ll find one of our locations to be just around the corner: Town ‘n’ Country, West Tampa, Westchase, Tampa, Pebble Creek, New Tampa, Wesley Chapel, Lutz, Temple Terrace, East Tampa, near USF, South Tampa, Hyde Park, Davis Island and Bayshore. 
			</p>
			<p>
				Of course, even if you’re from further away or out-of-state, you’ll still find a warm welcome! 
			</p>
			<h2>Is it your first time visiting us?</h2>
			<p><a href="/what-to-expect/">See what you can expect on your first visit.</a></p>


		</div><!-- content -->

<?php get_footer(); ?>