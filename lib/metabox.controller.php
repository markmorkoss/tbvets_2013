<?php
class Metabox {
	var $test;
	var $group;
	var $fields;
	var $instance_count;
	var $name;

	function __construct() {
 
	}
    
    function set_group($group) {
		$this->group = $group;
    }
    
    function get_group() {
    	return $this->group;
    }
    
    function get_name() {
    	global $wpdb;
    	$results = $wpdb->get_var($wpdb->prepare("SELECT label FROM $wpdb->field_groups WHERE `group_id` = '$this->group'"));
		return $results;
    }
      
    function get_group_values($group) {
    	global $wpdb; 
    	$results = $wpdb->get_results($wpdb->prepare("SELECT * $wpdb->field_groups WHERE `group_id` = '$group'"));
    	return $results;
    }
    
	function set_field_key($new_test) {
		$this->test = $new_test;
	}
	
	function get_field_key() {
		return $this->test;
	}

	function get_fields() {
		global $wpdb;
		$fields = $wpdb->get_results("SELECT * FROM `wp_posts_fields` WHERE `group` = ".$this->group ." ORDER BY `display_order`");
		return $fields;
	}
	
	function count_fields() {
		$count = count($this->fields);
		return $count;
	}
	
	function get_max($post_id) {
		foreach($this->fields as $field) {
			$vals[] = get_post_meta($post_id, $field->key, false);
			$counts[] = count(get_post_meta($post_id, $field->key, false));
			$tcount[] = max($counts);
		} 
		$count = max($tcount);
		if($count < 1) { $count = 1; }
		$this->instance_count = $count;
		return $count;
	}
	
	function make_form_field($field, $old, $i) {
		?>
		<div class="meta-field <?php echo $field->class; ?> <?php echo $field->key; ?>">
		<div id="label">
		<label for="<?php echo $field->key; ?>" style="width:20%;margin:10px 5px;"><?php echo $field->label; ?></label>
		</div>
		<?php if ($field->type == 'select') { 
			$options = $field->options; 
			$options = (array)maybe_unserialize($options);
			$options = (array)maybe_unserialize($options[0]);	
		?>
			<div id="form-field" class=" ">
			<select id="<?php echo $field->key; ?>" name="<?php echo $field->key; ?>" class="<?php echo $field->class; ?>">
			<option value="">Select...</option>
			<?php foreach($options as $option) { ?>
			<option value="<?php echo $option; ?>" <?php if($option == $old) {echo 'selected';} ?>><?php echo $option; ?></option>
			<?php } ?>
			</select><br/>
			</div>
		<?php } elseif($field->type == 'term') { ?>
			<div id="form-field" class=" ">
			<?php 
			$old = wp_get_post_terms($post->ID, $field->key); 
			foreach($old as $old_val) { $old = $old_val->slug; }
			?>
			<select id="<?php echo $field->key; ?>" name="<?php echo $field->key; ?>" class="<?php echo $field->class; ?>">
			<option value="">Select...</option>
			<?php $terms = get_terms($field->key, array('hide_empty' => 0)); ?>
			<?php foreach($terms as $term) { ?>
			<?php if(!empty($old)) { $sel = ($old == $term->slug) ? 'selected': ''; } else { $sel = ($term->slug == "draft") ? 'selected': ''; } ?>
			<option value="<?php echo $term->slug; ?>" <?php echo $sel; ?>><?php echo $term->name; ?></option>
			<?php } ?>
			</select><br/>
			</div>
		<?php } elseif($field->type == 'related_post') { ?>
			<?php if($field->single == 'true') { ?>
			<div id="form-field" class=" ">
			<select id="<?php echo $field->key; ?>" name="<?php echo $field->key; ?>" class="<?php echo $field->class; ?>">
			<option value="">Select...</option>
			<?php 
			$pt = $field->options['post_type'];
			$posts = get_posts('post_type='. $pt); 
			?>
			<?php foreach($posts as $post) { ?>
			<?php if(!empty($old)) { $sel = ($old == $post->ID) ? 'selected': ''; } ?>
			<option value="<?php echo $post->ID; ?>" <?php echo $sel; ?>><?php echo $post->post_title; ?></option>
			<?php } ?>
			</select><br/>
			</div>
			<?php } else { ?>
			<?php } ?>
		<?php } elseif($field->type == 'date') { ?>
			<?php 
			//set up variables
			$list = get_post_meta($post->ID,$mkey,true); 
			$list = explode('-',$list);
			$lmo = $list[1];
			$ldy = $list[2];
			$lyr = $list[0];
			?>
			<div id="form-field">
			<div class="date-field">
			<select name="<?php echo $mkey. '-mo'; ?>" id="<?php echo $mkey. '-mo'; ?>" class="<?php echo $field->class; ?>">
			<option value="">Select Month ...</option>
			<option value="01" <?php if($lmo == '01') {echo 'selected'; } ?>>Jan</option>
			<option value="02" <?php if($lmo == '02') {echo 'selected'; } ?>>Feb</option>
			<option value="03" <?php if($lmo == '03') {echo 'selected'; } ?>>Mar</option>
			<option value="04" <?php if($lmo == '04') {echo 'selected'; } ?>>Apr</option>
			<option value="05" <?php if($lmo == '05') {echo 'selected'; } ?>>May</option>
			<option value="06" <?php if($lmo == '06') {echo 'selected'; } ?>>Jun</option>
			<option value="07" <?php if($lmo == '07') {echo 'selected'; } ?>>Jul</option>
			<option value="08" <?php if($lmo == '08') {echo 'selected'; } ?>>Aug</option>
			<option value="09" <?php if($lmo == '09') {echo 'selected'; } ?>>Sep</option>
			<option value="10" <?php if($lmo == '10') {echo 'selected'; } ?>>Oct</option>
			<option value="11" <?php if($lmo == '11') {echo 'selected'; } ?>>Nov</option>
			<option value="12" <?php if($lmo == '12') {echo 'selected'; } ?>>Dec</option>
			</select>
			<select name="<?php echo $mkey . '-dy'; ?>" id="<?php echo $mkey. '-dy'; ?>" class="<?php echo $field->class; ?>">
			<?php $i = 1; while($i <= 31) {
			if($i == "$ldy") { $selected = 'selected'; } else { $selected = '';}
			if($i < 10) { 
			$y = sprintf('%02d',$i); 
			echo '<option value="' .$y .'" ' .$selected .'>'.$y.'</option>';
			} else {
			echo '<option value="'.$i .'" ' .$selected .'>' .$i .'</option>'; 
			}
			$i++;
			} 
			?>
			</select>
			<select name="<?php echo $mkey . '-yr'; ?>" id="<?php echo $mkey. '-yr'; ?>" class="<?php echo $field->class; ?>">
			<?php $i = 2009; while($i >= 2009 && $i <= 2015) { 
			if($i == "$lyr") { $selected = 'selected'; } else { $selected = '';}
			echo '<option value="'.$i .'" ' .$selected .'>' .$i .'</option>'; $i++;} ?>
			</select>
			</div>
			</div>
			
		<?php } elseif($field->type == 'check') { ?>
			
			<?php if($field->single ==  true) { ?>
				<div id="form-field">
					<?php $old = get_post_meta($post->ID, $mkey, true); ?>
					<?php foreach($field->options as $option) { ?>
					<input id="<?php echo $field->key; ?>" type="checkbox" name="<?php echo $field->key; ?>" value="<?php echo $option; ?>" <?php if($old == $option) {echo 'checked';} ?> class="<?php echo $field->class; ?>"/>
					<?php echo $option; ?> 
					<?php } ?>
				</div>
			<?php } else { ?>
				<div id="form-field">
					<?php $old = get_post_meta($post->ID,$mkey, false); ?>
					<?php foreach($field->options as $option) { ?>
					<input id="<?php echo $field->key; ?>" type="checkbox" name="<?php echo $field->key .'[]'; ?>" value="<?php echo $option; ?>" <?php if(in_array($option,$old)) {echo 'checked';} ?> class="<?php echo $field->class; ?>"/>
					<?php echo $option; ?> 
					<?php } ?>
				</div>
			<?php } ?>
			
		<?php } elseif($field->type == 'text') { ?>
		<?php if(!$field->class) { $field->class = 'wide'; } ?>
			<div id="form-field">
			<input id="<?php echo $field->key; ?>" type="text" name="<?php echo $field->key; ?><?php if($field->single == 1) {echo "[$i]";} ?>" value="<?php print_r($old); ?>" class="<?php echo $field->class; ?>" /><br/>
			</div>
		<?php } elseif($field->type == 'textarea') { ?>
			<div id="form-field">
			<textarea id="<?php echo $field->key; ?>" name="<?php echo $field->key; ?>" value="" rows="5" cols="25" class="<?php echo $field->class; ?>"><?php echo $old; ?></textarea><br/>
			</div>
		<?php } else { ?>
		<?php } ?>
		</div>
		<?php
	} // end function
	
	function delete_meta_data($post_id) {
		foreach($this->fields as $field) {
			delete_post_meta($post_id, $field['name']);
		}
	}
	
	function save_meta_field($post_id,$key,$type,$new,$old) {
		if($type == 'term') {
			wp_set_object_terms($post_id, $new, $key, false );
		} elseif($type == 'date') {
			$date = array($_POST[$key .'-yr'],$_POST[$key .'-mo'],$_POST[$key .'-dy']);
			$date = implode('-',$date);	
			update_post_meta($post_id, $key, $date, $old);
		} elseif(is_array($new)) {
			foreach($new as $new_value) {
				add_post_meta($post_id,$key,$new_value);
			}
		} else {
			if($old == $new) {
			//Do Nothing
			} elseif($old == true && $new != true) {
				delete_post_meta($post_id, $key, $old);
			} else {
				update_post_meta($post_id,$key,$new);
			}			
		}
	}

} // end class
?>
