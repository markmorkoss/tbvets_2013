<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<div id="main" class="content-trails">

		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<?php
			global $post;
			$parent = $post->post_parent;
			?>

			<section class="supporting">
				<div class="img"><?php the_post_thumbnail(array(220,220),array('class'=>'post-image bio-image frame')); ?></div>
				<h1><?php the_title(); ?></h1>
				<ul>
					<li>Hometown: <?php echo get_post_meta( $post->ID, 'tbvets_hometown', true); ?></li>
					<li>Employee since: <?php echo date("Y" , strtotime(get_post_meta( $post->ID, 'tbvets_staff_years', true) ) ); ?></li>
					<li>Find me at: <?php $locations = get_related_item($post->ID, 'location', array('post_title','ID','guid'), false);
						$i = 0;
						foreach($locations as $location) :
							$pre = ($i == 0) ? ' ' : ', ';
							echo $pre. '<a href="'.$location->guid .'">'.$location->post_title .'</a>';
							$i++;
						endforeach;
						?>
					</li>
				</ul>
				<p><a href="/staff/">&larr; back to all staff profiles</a></p>			
			</section>
		
			<div class="content" class="staff-bio">
				<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
			</div><!-- #content -->
		<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>