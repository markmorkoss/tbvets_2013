<?php
/**
Template Name: Services Page	
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

	<div id="main" class="content-trails">
	
		<section class="supporting">
			<h2>Pet Services</h2>
			<?php $terms = get_posts(array('post_type' => 'services', 'post_parent'=> 0, 'numberposts' => '-1', 'orderby'=> 'title','order'=>'ASC')); ?>
			<?php
				foreach($terms as $parent) : 
					$posts = get_posts(array('post_parent' => $parent->ID, 'post_type' => 'services','numberposts'=>'-1','orderby'=> 'title','order'=>'ASC') );?>	
					<h2 class="service-parent"><?php echo $parent->post_title; ?></h2>
					<ul>
						<?php foreach($posts as $post) : ?>
							<li><a href="<?php echo $post->post_name; ?>"><?php echo $post->post_title; ?></a></li>
						<?php endforeach; ?>
					</ul>
			<?php endforeach; ?>
		</section><!-- sidebar -->
	
		<div class="content">
			<h2>Tampa Vet Services, A-Z:</h2>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php $services = get_posts(array('post_type' => 'services', 'numberposts'=> '-1', 'orderby' => 'title', 'order' => 'asc'));
				foreach($services as $service) : if($service->post_parent == true)  :
					?>
                    <div class="service">
						<div class="service-icon">
							<a name="<?php echo $service->post_name; ?>"></a>
								<a href="<?php echo $service->guid; ?>">
								<?php if(has_post_thumbnail($service->ID)) : ?>
								<?php echo get_the_post_thumbnail($service->ID,array(150,150),array('class'=>'alignright service-thumb frame')); ?>
							<?php else: $icon = 'http://vets.bigseapreview.com/wp-content/uploads/2010/08/service-image.jpg'; ?>
								<img src="<?php echo $icon; ?>" class="alignright service-thumb frame" alt="<?php echo $service->post_title; ?>">
							<?php endif; ?>
								</a>
                            
                            <h2><a href="<?php echo $service->guid; ?>" title="<?php echo $service->post_title; ?>"><?php echo $service->post_title; ?></a></h2>
						
						</div>
					
						<p class="service-content"><?php echo $service->post_excerpt; ?><br />
                          <a href="<?php echo $service->guid; ?>" title="<?php echo $service->post_title; ?>" class="learn-more"> Learn More</a></p>
                      
						<br class="clear" />
					</div>
					
				<?php endif; endforeach; ?>
			</div><!-- #post-## -->
		
			<br class="clear" />
	
		</div><!-- #content -->

<?php get_footer(); ?>