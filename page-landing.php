<?php
/**
Template Name: Landing Page
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage tbvets
 */

 ?>
 <!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
	    <link href="http://www.tampabayvets.net/wp-content/themes/tbvets_2013/landing.css" rel="stylesheet" type="text/css">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>
        	Tampa Animal and Bird Hospitals<?php
			/*
			 * Print the <title> tag based on what is being viewed.
			 */
			global $page, $paged;

			wp_title( 'Tampa Vet |', true, 'right' );

			// Add the blog name.
			bloginfo( 'name' );

			// Add the blog description for the home/front page.
			$site_description = get_bloginfo( 'description', 'display' );
			if ( $site_description && ( is_home() || is_front_page() ) )
				echo " | $site_description";

			// Add a page number if necessary:
			if ( $paged >= 2 || $page >= 2 )
				echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

			?></title>
		    <meta name="robots" content="index, follow" />
		    <meta content="Tampa Vet with six Tampa locations in Westchase / Town-n-Country, Temple Terrace, New Tampa, Lutz and South Tampa.  Full-service animal hospital.  High-quality, top-of-the-line diagnostics and medial facilities, caring and compassionate staff." name="description">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_directory' ); ?>/css/normalize.min.css">
        <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_directory' ); ?>/css/main.css">

        <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>

		<script src="http://use.typekit.com/xlu2tzd.js"></script>
		<script>try{Typekit.load();}catch(e){}</script>
		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-8942809-5', 'auto');
  ga('send', 'pageview');

</script>

		<?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

			<div id="wrapper">

				<header role="banner">

					<h1><a href="<?php bloginfo( 'url' ); ?>" title="Tampa Animal and Bird Hospital Homepage">Tampa Animal and Bird Hospital</a></h1>

				

					<?php include('searchform.php'); ?>

					<nav role="navigation">
						<?php wp_nav_menu( array( 'menu' => 'primary', 'container' =>'ul' ) ); ?>

						<!--
						<h4>Locations:</h4>
						<ul class="locations">
							<li><a href="/location/temple-terrace/">Temple Terrace</a></li>
							<li><a href="/location/pebble-creek/">Pebble Creek</a></li>
							<li><a href="/location/north-bay/">North Bay</a></li>
							<li><a href="/location/cat-doctors/">Cat Doctors</a></li>
							<li><a href="/location/all-creatures/">All Creatures</a></li>
							<li><a href="/location/cheval/">Cheval</a></li>
						</ul>
						-->
					</nav>

    			<span class="toggle">
    				<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/img/navbars.png" alt="" />
    				Site Menu &amp; Locations
    			</span>

				</header><!-- end header/banner -->

	<div id="main" class="content-leads">
		<div class="content">
		<?php 
			global $post;
			query_posts('page_id='.$post->ID);
			while (have_posts()) : the_post(); 
			?>
			<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<?php else :
$image = get_bloginfo( 'stylesheet_directory') . '/images/default_cat_img.jpg'; ?>
<?php endif; ?>
<div id="category-name" style="background-image: url('<?php echo $image[0]; ?>')"><div class="overlay">
<h1><?php the_title(); ?></h1>
<?php the_content(); ?>



</div>
</div>
			
						
			
			<?php endwhile; ?>
		</div>

<?php get_footer(); ?>