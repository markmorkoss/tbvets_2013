<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

	<div id="main" class="content-leads">
		<div class="content">

			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1>Ask a Vet:<br />
					<strong><?php the_title(); ?></strong></h1>
	
					<div class="entry-content">
					<?php the_post_thumbnail('medium',array('class' => 'alignright frame')); ?>
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
					</div><!-- .entry-content -->
	
					<div class="entry-meta">
						Published on <?php the_time(get_option('date_format')); ?>
					</div><!-- .entry-meta -->
	
				</div><!-- #post-## -->

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
		
		<section class="supporting">
			<h3>Have a pet issue?</h3>
			<p><a href="/ask-a-vet/" class="green-button">Ask a vet your question</a></p>

			<hr />
			
			<?php 
			if(isset($_GET['q_submit'])) { $q = $_GET['q_search']; }
			if(isset($_GET['qcat'])) { $qcat = $_GET['qcat']; }
			?>
			<form id="q-search" action="<?php self_link(); ?>" method="get" id="searchform">
				<input type="text" name="q_search" value="search questions..." onfocus="clearText(this)" />
				<input type="submit" value="search" name="q_submit" />
			</form>



			<h3>More...</h3>
			<p class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '', 'Previous question', 'twentyten' ) . '</span> next question: %title' ); ?></p>
			<p class="nav-next"><?php next_post_link( '%link', '<span class="meta-nav">' . _x( '', 'Next question', 'twentyten' ) . '</span> previous question: %title' ); ?></p>

		</section>

<?php get_footer(); ?>