<?php
/**
Template Name: Work With Us
 *
  *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

	<div id="main" class="content-trails">
	
	<section class="supporting">
	<?php 
	$loc = $_GET['lid'];
	if(isset($loc)) { 
		$loc = intval($loc);
		$loc_name = &get_post($loc);
		 ?>
				<h1>Location:<br /><?php echo $loc_name->post_title; ?></h1>
				<p><a href="<?php bloginfo('url'); ?>/staff/" title="Back to full staff list">&larr; Back to all staff</a></p>

				<div class="intro">
						<a href="<?php bloginfo('url'); ?>/location/<?php echo $loc_name->post_name; ?>/" title="<?php echo $loc_name->post_title; ?>" ><?php echo get_the_post_thumbnail($loc_name->ID, array(160,200), array('class'=>'frame')); ?></a>
						</div>
						
						<div class="info">
						<strong><a href="<?php bloginfo('url'); ?>/location/<?php echo $loc_name->post_name; ?>/"><?php echo $loc_name->post_title; ?></a> <span class="tiny">(<?php echo get_post_meta($loc_name->ID, 'tbvets_subtitle', true); ?>)</span></strong>
						<p class="address"><?php echo get_post_meta($loc_name->ID,'tbvets_address1',true); ?>
						<?php if((get_post_meta($loc_name->ID,'tbvets_address2',true))) { ?><?php echo get_post_meta($loc_name->ID,'tbvets_address2',true); ?><?php } ?>
						<?php echo get_post_meta($loc_name->ID,'tbvets_city',true); ?>, <?php echo get_post_meta($loc_name->ID,'tbvets_state',true); ?>  <?php echo get_post_meta($loc_name->ID,'tbvets_zip',true); ?> <?php $check = get_post_meta($loc_name->ID,'tbvets_map',$true); if($check): ?><span class="map-link"><small><a href="<?php echo htmlentities(get_post_meta($loc_name->ID,'tbvets_map',true)); ?>">(View map)</a></small></span><?php endif; ?></p>
						
						<p class="hours"><strong>Hours:</strong><br />
						<?php echo nl2br(strip_tags(get_post_meta($loc_name->ID,'tbvets_hours',true))); ?></p>
	
				</div>
		<?php } else { ?>
		<h1>View Staff by Location:</h1>
		<div class="staff_by_location">
		<?php
		$locs = get_posts(array('post_type' => 'location'));
		foreach($locs as $loc_name):
			?>
			<div class="location">
				<h2><a href="<?php self_link(); ?>?lid=<?php echo $loc_name->ID; ?>"><?php echo $loc_name->post_title; ?></a></h2>
				<p><a href="<?php self_link(); ?>?lid=<?php echo $loc_name->ID; ?>"><?php echo get_the_post_thumbnail($loc_name->ID, array(160,200), array('class'=>'frame')); ?></a>
					<br />
					<?php echo get_post_meta($loc_name->ID,'tbvets_address1',true); ?>, <?php echo get_post_meta($loc_name->ID,'tbvets_city',true); ?>
				</p>
			</div>
		<?php endforeach; ?>
		</div>
		<?php } ?>
	</section>
	
	<div class="content">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php if ( is_front_page() ) { ?>
						<h2 class="entry-title"><?php the_title(); ?></h2>
					<?php } else { ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
					<?php } ?>
                 	<?php the_post_thumbnail('medium',array('class' => 'alignright frame')); ?>
                    <?php the_content(); ?>

			<?php endwhile; ?>
        
						</div>							
					</div>

	</div><!-- #content -->

<?php get_footer(); ?>