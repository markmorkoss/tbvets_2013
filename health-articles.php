<?php
/**
 Template Name: Pet Health Articles
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>
	<div id="main" class="content-leads">
		<div class="content">
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<h1><?php the_title(); ?></h1>
				<div class="entry-content">
				 <script language="javascript">
					function updateIFrame(h) {
						var o=document.getElementById("petHealth");
						o.setAttribute("height",h);
					}
					window.onload=function() {
						location.href='#';
					};
					$('iframe').iFrameResize();
				</script>
				<iframe id="petHealth" name="petHealth" src="http://www.lifelearn-cliented.com/iframe.php?clinic=2887" width="100%" height="700" border="0"></iframe>
			</div><!-- .entry-content -->
		</div><!-- #post-## -->
	</div><!-- #content -->

	<?php //get_sidebar(); ?>

	<section id="mainbar-health" class="ask-box supporting">
<p> <a href="http://www.tampabayvets.net/handouts/" class="green-button location-button">Health Focus Handouts</a> </p>

		<h1>Ask a Vet</h1>
		<p>
			Pet ownership isn&#8217;t always easy &mdash; but we&#8217;re here to help. Noticed a behavior change in your animal recently? Not sure if you need to bring them in for a visit, or just wait it out? Send us your questions.
		</p>

		<p>
			<img src="/wp-content/themes/tbvets/images/staff.jpg" width="225" />
		</p>

		<h3>Ask the vets your question</h3>
		<?php
			/*if(isset($_POST['submit']))
			{
				$data = array(
				'post_title' => $_POST['qtitle'], 'email' => $_POST['qemail'], 'name' => $_POST['qname']
				);
				add_question($data);
			}
			?>
			<form action="" method="post">
			<textarea name="qtitle"></textarea>
			<p><input type="text" name="qname" value="Your name" /></p>
			<p><input type="text" name="qemail" value="your@email.com" /></p>
			<input type="submit" name="submit" value="Send" class="green-button" />
			</form>*/
		gravity_form(1, false, false, false, '', false);
		?>
<p><strong>Want to learn more about how to keep your feline friend healthy and happy?</strong></p>
<p><a href="https://catfriendly.com/cat-care-at-home/" target="_blank" class="green-button location-button">Cat Care at Home</a></p>
<p><a href="https://catfriendly.com/keep-your-cat-healthy/" target="_blank" class="green-button location-button">Keep Your Cat Healthy</a></p>
<p><a href="https://catfriendly.com/feline-diseases/" target="_blank" class="green-button location-button">Diseases</a></p>
<p><a href="https://catfriendly.com/why-does-my-cat/" target="_blank" class="green-button location-button">Why Does My Cat … ?</a></p>
<p><a href="https://catfriendly.com/be-a-cat-friendly-caregiver/" target="_blank" class="green-button location-button">Be a Cat Friendly Caregiver</a></p>
<p><a href="https://catfriendly.com/toybox/" target="_blank" class="green-button location-button">The Toy Box</a></p>


	</section>

<?php get_footer(); ?>
