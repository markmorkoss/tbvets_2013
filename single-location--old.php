<?php
/**
 * Single location template.
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<?php global $post; $post_type = $post->post_type; $lid = $post->ID; $lslug = $post->post_name; ?>

	<div id="main" role="main" class="content-leads location-page">
			
		<header class="<?php echo $post->post_name; ?>"><!-- see locations.scss for other location classes -->
		
			<small>Location:</small>
			<h1><?php echo $post->post_title; ?></h1>
			
			<nav>
				<ul>
					<?php wp_nav_menu(array('menu' => $post->post_title) ); ?>
				</ul>
			
			</nav>
		
		</header>

	<div class="content">
					
		<section class="location-details">
			
			<div class="location-slides">
				<div class="slides-wrapper" data-autorotate="3000">
					<ul class="slider">
						<?php $imgs = get_posts('post_type=attachment&post_parent='.$post->ID); 
							foreach($imgs as $img) {
								echo '<li class="slide">' .wp_get_attachment_image($img->ID, array(320,200)) .'</li>'; 
							} 
						?>
					</ul>
				</div>
			</div>
			
			<div class="office-loc">
				<div class="office-loc-map">
					<?php echo get_post_meta($post->ID,'tbvets_google',true); ?>
				</div>

				<div class="office-loc-details">
					<p class="address">
						<?php echo get_post_meta($post->ID,'tbvets_address1',true); ?>
						<?php if((get_post_meta($post->ID,'tbvets_address2',true))) { ?><?php echo get_post_meta($post->ID,'tbvets_address2',true); ?><?php } ?><br />
						<?php echo get_post_meta($post->ID,'tbvets_city',true); ?>, <?php echo get_post_meta($post->ID,'tbvets_state',true); ?>  <?php echo get_post_meta($post->ID,'tbvets_zip',true); ?> 
						
					</p>
					
					<div class="pet-portal-btn">
						<a href="http://www.myallypage.com" target="_blank">
							<span class="large-text">Pet Portal - Log In</span><br />
							<span class="small-text">Manage your pet's health online</span>
						</a>
					</div>
		
					
					<ul class="buttons">
						<li><a href="http://www.tampabayvets.net/popup-appointment/" class="green-button" class="lightwindow">Schedule Appointment</a></li>
						<li><a href="http://www.tampabayvets.net/forms" class="green-button">Patient Forms</a></li>
						<li><a href="<?php echo get_post_meta($post->ID, 'tbvets_pharma', true); ?>" class="green-button" title="Online Pharmacy for <?php echo $post->post_title; ?>" target="_blank">Online Pharmacy</a></li>
					</ul>
					
				</div>
			
			</div>
			
			<div class="loc-info">
				
				<h1><?php the_title(); ?></h1>
				
				<div class="info">
				
					<h3>Hours</h3>
					<p><?php echo nl2br(strip_tags(get_post_meta($post->ID,'tbvets_hours',true))); ?></p>
				</div>
				<div class="info">
					<h3>Contact Us</h3>
					<p><?php echo get_post_meta($post->ID,'tbvets_phone',true); ?><br />
					<?php echo get_post_meta($post->ID,'tbvets_email',true); ?></p>
				</div>
			
			</div>
			<div class="clear">&nbsp;</div>
			
		</section><!-- end location-details -->
		
		<div class="intro">
		
			<?php the_content(); ?>

		</div><!-- end intro -->
	
	</div><!-- end content -->

	<section class="supporting">
	
		<a class="fan" href="http://www.facebook.com/AnimalHospitalsTampa">Find us on <span>Facebook</span></a>
	
		<div class="staff-profiles">
		
			<h2>Staff Profiles</h2>
			
			<p>Our team members strive to meet and exceed the needs of every client and pet that visits us. Our vets are <strong><a href="http://www.abvp.com/" target="_blank">American Board of Veterinary Practitioners (ABVP) certified</a></strong> in canine, feline and avian and exotic practice.</p>
			
			<ul class="staff">
				<?php 
					$items = get_related_item($lid,'staff',array('ID','menu_order'), 'menu_order ASC');
					foreach($items as $item):
				?>
					<?php if($item->menu_order < 20): ?>
					<a href="<?php echo get_permalink($item->ID); ?>"></a>
					<li><a href="<?php echo get_permalink($item->ID); ?>"><?php echo get_the_post_thumbnail($item->ID, array(95,95) , array('class'=>'tile frame wp-post-image')); ?></a></li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
			
			<a class="button" href="<?php bloginfo('url'); ?>/staff/?lid=<?php echo $lid; ?>" title="Our entire staff">View all Staff Profiles &rarr;</a>
		
		</div>
	
	</section><!-- end supporting -->
	
	<?php endwhile; ?>

<?php get_footer(); ?>