<?php
add_action('admin_menu', 'location_add_custom_box');
add_action('save_post', 'location_save_postdata');

function location_add_custom_box() {
  if( function_exists( 'add_meta_box' )) {
    add_meta_box( 'location_sectionid', __( 'Location Info', 'location_textdomain' ), 
                'location_meta_box', 'location', 'normal','high' );
  }
}

function location_meta() {
	$meta = array(
	'tbvets_subtitle' => array('key' => 'tbvets_subtitle', 'label' => "Location Subtitle", 'single' => 'true', 'type' => 'text', 'class' => 'clear'),
	'tbvets_phone' => array('key' => 'tbvets_phone', 'label' => "Phone #", 'single' => 'true', 'type' => 'text', 'class' => 'clear'),
	'tbvets_email' => array('key' => 'tbvets_email', 'label' => "Contact Email", 'single' => 'true', 'type' => 'text', 'class' => 'no-clear'),
	'tbvets_address1' => array('key' => 'tbvets_address1', 'label' => "Address", 'single' => 'true', 'type' => 'text', 'class' => 'clear'),
	'tbvets_address2' => array('key' => 'tbvets_address2', 'label' => "Address (line 2)", 'single' => 'true', 'type' => 'text', 'class' => 'clear'),
	'tbvets_city' => array('key' => 'tbvets_city', 'label' => "City", 'single' => 'true', 'type' => 'text', 'class' => 'clear'),
	'tbvets_state' => array('key' => 'tbvets_state', 'label' => "State", 'single' => 'true', 'type' => 'text', 'class' => 'no-clear'),
	'tbvets_zip' => array('key' => 'tbvets_zip', 'label' => "Zip", 'single' => 'true', 'type' => 'text', 'class' => 'no-right'),
	'tbvets_pharma' => array('key' => 'tbvets_pharma', 'label' => "Pharmacy Link", 'single' => 'true', 'type' => 'text', 'class' => 'clear'),
	'tbvets_hours' => array('key' => 'tbvets_hours', 'label' => "Hours", 'type'=> 'textarea', 'class' => 'clear')
	);
	return $meta;
}


function location_meta_box() {
  echo '<input type="hidden" name="location_noncename" id="location_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

  // The actual fields for data entry
  global $post
  ?>
<div id="metabox-location">
	<?php	
	$metas = location_meta(); 
	foreach($metas as $key) {
	$mkey = $key['key'];
	$old = get_post_meta($post->ID,$mkey,true);
	?>
<div class="meta-field <?php echo $key['class']; ?> <?php echo $key['key']; ?>">
	<div id="label">
			<label for="<?php echo $key['name']; ?>" style="width:20%;margin:10px 5px;"><?php echo $key['label']; ?></label>
		</div>
 
	<?php if ($key['type'] == 'select') { ?>
   	<div id="form-field" class=" ">
  		<select id="<?php echo $key['key']; ?>" name="<?php echo $key['name']; ?>">
		<option value="">Select...</option>
				<?php foreach($key['options'] as $option) { ?>
				<option value="<?php echo $option; ?>" <?php if($option == $old) {echo 'selected';} ?>><?php echo $option; ?></option>
				<?php } ?>
			</select><br/>
	</div>
	<?php } elseif($key['type'] == 'check') { ?>
	 	<div id="form-field" class="">
			<?php $old = get_post_meta($post->ID,$mkey, false); ?>
			<ul style="margin:10px;">
			<?php foreach($key['options'] as $option) { ?>
			<li><input id="<?php echo $key['key']; ?>" type="checkbox" name="<?php echo $key['name'] . '[]'; ?>" value="<?php echo $option; ?>" <?php if(in_array($option,$old)) {echo 'checked';} ?>/>
			<?php echo $option; ?> 
			</li>
			<?php } ?>
		</div>
	<?php } elseif($key['type'] == 'text') { ?>
		 	<div id="form-field" class="">
			<input id="<?php echo $key['key']; ?>" type="text" name="<?php echo $key['name']; ?>" value="<?php echo $old; ?>" /><br/>
			</div>
	<?php } elseif($key['type'] == 'textarea') { ?>
		 	<div id="form-field">
			<textarea id="<?php echo $key['key']; ?>" name="<?php echo $key['name']; ?>" value="" rows="5" cols="25" ><?php echo $old; ?></textarea><br/>
			</div>
	<?php } else { ?>
	<?php } ?>
</div>
<?php } ?>
<div style="clear:both;"></div>
</div>
<?php
}
/* When the post is saved, saves our custom data */
function location_save_postdata( $post_id ) {
$metas = location_meta();
    if ( !wp_verify_nonce( $_POST['location_noncename'], plugin_basename(__FILE__) )) {
    return $post_id;
  	}
	
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
    return $post_id;
	}
 
  if ( 'page' == $_POST['post_type'] ) {
    if ( !current_user_can( 'edit_page', $post_id ) )
      return $post_id;
  } else {
    if ( !current_user_can( 'edit_post', $post_id ) )
      return $post_id;
  }

   
//Single Metas
	foreach($metas as $val) {
		$key = $val['name'];
		$type = $val['type'];
		$val = $_POST[$key];
			if(is_array($val)) {
				delete_post_meta($post_id,$key);
				foreach($val as $new_value) {
					add_post_meta($post_id,$key,$new_value);
					}
			} else {
				$old = get_post_meta($post_id, $key, true); 
				$new = $val;
					if($old == $new) {
						//Do Nothing
					} elseif($old == true && $new != true) {
						delete_post_meta($post_id, $key, $old);
					} else {
						update_post_meta($post_id,$key,$new);
					}
			}
		}
  return $mydata;
}

function meta_style() {
	if(is_admin()) {
		$src = get_bloginfo('siteurl') .'/wp-content/themes/tbvets/meta-box.css'; 
		wp_register_style('metabox', $src);
		wp_enqueue_style('metabox');
		}
}
add_action('init','meta_style');

?>