<?php
/**
 * Single location template.
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
	<?php global $post; $post_type = $post->post_type; $lid = $post->ID; ?>

	<div id="main" role="main" class="content-leads location-page">

		<header class="<?php echo the_title(); ?>"><!-- see locations.scss for other location classes -->

			<small>Location:</small>
			<h1><?php echo the_title(); ?></h1>

			<nav>
				<ul>
					<?php wp_nav_menu(array('menu' => $post->post_title) ); ?>
				</ul>

			</nav>

		</header>

	<section class="location-details">
		<div class="location-left">
			<ul class="buttons">
				<li><a href="http://www.tampabayvets.net/forms" class="blue-button">Patient Forms</a></li>
				<li><a href="<?php echo get_post_meta($post->ID, 'tbvets_pharma', true); ?>" class="blue-button" title="Online Pharmacy for <?php echo $post->post_title; ?>" target="_blank">Home Delivery Store</a></li>
				
			</ul>

			<div class="location-slides">
				<?php $message = get_field('message'); ?>
				<?php if ($message): ?>
					<h3><?php echo $message; ?></h3>
				<?php endif; ?>
				<iframe width="100%" height="320" src="<?php the_field('embed_video'); ?>" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="location-right">
			<a href="http://www.tampabayvets.net/hours-and-locations/" class="green-button location-button">Schedule Appointment</a>

			<div class="info">

				<p class="map">
					<?php echo get_post_meta($post->ID,'tbvets_address1',true); ?>
					<?php echo (get_post_meta($post->ID,'tbvets_address2',true) ? get_post_meta($post->ID,'tbvets_address2',true) : ''); ?>
					<?php echo (get_post_meta($post->ID,'tbvets_map',true) ? '&nbsp;<a href="'.get_post_meta($post->ID,'tbvets_map',true).'" target="_blank" class="tiny">(map)</a>' : ''); ?><br />
					<?php echo get_post_meta($post->ID,'tbvets_city',true); ?>, <?php echo get_post_meta($post->ID,'tbvets_state',true); ?>  <?php echo get_post_meta($post->ID,'tbvets_zip',true); ?>
				</p>
				<p class="phone"><?php echo get_post_meta($post->ID,'tbvets_phone',true); ?></p>
				<p class="email"><?php echo get_post_meta($post->ID,'tbvets_email',true); ?></p>
			</div>

			<div class="hours">
				<h3>Hours</h3>
				<?php echo get_post_meta($post->ID,'tbvets_hours',true); ?>
			</div>
			<div class="coupon">
				<h3 style="text-align: left;margin-bottom:5px !important;">First time client?</h3>
				<p style="text-align:left;margin-top: 0;">
					Receive <strong>$50 off</strong> Comprehensive Physical Exam for your dog or cat on your first visit!
					<br/>
					<a class="green-button" target="_blank" href="https://www.tampabayvets.net/wp-content/uploads/2020/03/gift-cert-2020-50off.pdf">Click here for $50 OFF 1st Exam!</a>
				</p>
			</div>
		</div>
	</section>

	<div class="content">

		<div class="intro">
			<div class="location-details">
				<div class="location-slides">
					<div class="slides-wrapper" data-autorotate="3000">
						<ul class="slider">
							<?php $imgs = get_posts('post_type=attachment&post_parent='.$post->ID. '&orderby=menu_order&order=ASC');
								foreach($imgs as $img) :
									echo '<li class="slide">' .wp_get_attachment_image($img->ID, array(635,426)) .'</li>';
								endforeach;
								wp_reset_postdata();
							?>
						</ul>
					</div>
				</div>
			</div>
			<?php the_content(); ?>

		</div><!-- end intro -->

		<div class="services-at-location">
			<h2>Services at <?php the_title(); ?>:</h2>
			<?php
			$services = rpt_get_object_relation( $post->ID, array('services'));
			$services = implode(',',(array)$services);
			$posts = get_posts(array('post_type' => 'services','include'=> $services ));
			$count = 1;
			foreach($posts as $serv) :
				?>
				<div class="service-item <?php echo ( $count % 2 == 0 ? 'even' : 'odd') ?>">
					<a href="<?php bloginfo('siteurl'); ?>/services/<?php echo htmlentities($serv->post_name); ?>/">
					<?php if(has_post_thumbnail($serv->ID)) : ?>
						<?php echo get_the_post_thumbnail($serv->ID, array(70,70), array('class'=>'alignright')); ?>
					<?php endif; ?>
					</a>
					<h3><a href="<?php bloginfo('siteurl'); ?>/services/<?php echo htmlentities($serv->post_name); ?>/" title="<?php echo $serv->post_title; ?>"><?php echo $serv->post_title; ?></a></h3>
					<div class="description">
					<?php echo strip_tags(substr($serv->post_content,0,250)); ?> ... <a href="<?php echo htmlentities($serv->guid); ?>" title="<?php echo $serv->post_title; ?>">More info</a>.
					</div>
				</div>
			<?php $count++; endforeach; wp_reset_postdata();?>
		</div>

	</div><!-- end content -->

	<section class="supporting">

		<?php
		/**
		 * New condition to use the corresponding social links for each location
		 * [Facebook, Instagram]
		 */
		if (is_single('temple-terrace')) {
			echo '<a target="_blank" class="fan" href="https://www.facebook.com/templeterraceanimalhospital/">Find us on <span>Facebook</span></a>';
			echo '<a target="_blank" class="inst" href="https://www.instagram.com/templeterraceanimalhospital">Find us on <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a>';
		} elseif (is_single('north-bay')) {
			echo '<a target="_blank" class="fan" href="https://www.facebook.com/northbayanimalhospital/">Find us on <span>Facebook</span></a>';
			echo '<a target="_blank" class="inst" href="https://www.instagram.com/northbayanimal">Find us on <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a>';
		} elseif (is_single('pebble-creek')) {
			echo '<a target="_blank" class="fan" href="https://www.facebook.com/pebblecreekanimalhospital/">Find us on <span>Facebook</span></a>';
			echo '<a target="_blank" class="inst" href="https://www.instagram.com/pebblecreekanimalhospital">Find us on <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a>';
		} elseif (is_single('cat-doctors')) {
			echo '<a target="_blank" class="fan" href="https://www.facebook.com/CatDoctors/">Find us on <span>Facebook</span></a>';
			echo '<a target="_blank" class="inst" href="https://www.instagram.com/catdoctors">Find us on <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a>';
		} elseif (is_single('all-creatures')) {
			echo '<a target="_blank" class="fan" href="https://www.facebook.com/allcreatureslutz/">Find us on <span>Facebook</span></a>';
			echo '<a target="_blank" class="inst" href="https://www.instagram.com/allcreaturesanimal/">Find us on <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a>';
		} elseif (is_single('cheval')) {
			echo '<a target="_blank" class="fan" href="https://www.facebook.com/chevalanimalhospital/">Find us on <span>Facebook</span></a>';
			echo '<a target="_blank" class="inst" href="https://www.instagram.com/chevalanimalhospital/">Find us on <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a>';
		} else {
			echo '<a target="_blank" class="fan" href="https://www.facebook.com/AnimalHospitalsTampa">Find us on <span>Facebook</span></a>';
		}
		?>



		<div class="staff-profiles">

			<h2>Staff Profiles</h2>

			<p>Our team members strive to meet and exceed the needs of every client and pet that visits us. Dr. Timothy Lassett and Dr. Link Welborn are <strong><a href="http://www.abvp.com/" target="_blank">American Board of Veterinary Practitioners (ABVP) certified</a></strong> in canine and feline practice.</p>

			<ul class="staff">
				<?php
					$items = get_related_item($lid,'staff',array('ID','menu_order'), 'menu_order ASC');
					foreach($items as $item):
				?>
					<?php if($item->menu_order < 20): ?>
					<a href="<?php echo get_permalink($item->ID); ?>"></a>
					<li><a href="<?php echo get_permalink($item->ID); ?>"><?php echo get_the_post_thumbnail($item->ID, array(95,95) , array('class'=>'tile frame wp-post-image')); ?></a></li>
					<?php endif; ?>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			</ul>

			<a class="button" href="<?php bloginfo('url'); ?>/staff/?lid=<?php echo $lid; ?>" title="Our entire staff">View all Staff Profiles &rarr;</a>

		</div>

	</section><!-- end supporting -->

	<?php endwhile; ?>

<?php get_footer(); ?>
