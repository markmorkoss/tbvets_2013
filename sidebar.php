<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage tbvets
 */
?>
<section id="primary" class="supporting widget-area" role="complementary">

<?php global $post; if( is_page('ask-a-vet') || get_post_type() == 'questions' ) { ?>
	
	<?php dynamic_sidebar('ask-a-vet-widget-area'); ?>

<?php } elseif($post_type == 'location') { ?>
	
	<div class="question">
		<?php $que = new WP_query('post_type=questions&orderbv=RAND&showposts=1'); ?>
		<?php while($que->have_posts()) : $que->the_post(); ?>
		<h2 class="question-header"><span class="q">Q.</span> <?php the_title(); ?></h2>
		<div class="question-content"><p><span class="a">A.</span> <?php the_content_rss('more', true , '', 150); ?></p></div>
		<?php endwhile; ?>
		<div class="q-link"><a href="<?php bloginfo('siteurl'); ?>/faq/" title="Read All Questions">Read All Questions</a></div>
	</div>
	
	<hr />
	
	<div class="staff">
		<h3>Staff Profiles</h3>
		<img src="<?php bloginfo('siteurl'); ?>/wp-content/themes/tbvets/images/staff.jpg" alt="View Staff Profiles" class="bigframe">
		<p>Check out our Staff Profiles</p>
	</div>
	
<?php } else { ?>
	
	<ul class="xoxo">
	
	<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	* the widgets for that widget area. If it instead returns false,
	* then the sidebar simply doesn't exist, so we'll hard-code in
	* some default sidebar stuff just in case.
	*/
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>
	
	<?php dynamic_sidebar('Ask a vet'); ?>
	
	<?php endif; // end primary widget area ?>
	</ul>

<?php } ?>

</section>
<!-- #primary .widget-area -->