<?php
/**
 * Realty Widget Class
 */
class VetsLocationWidget extends WP_Widget {
    function VetsLocationWidget() {
        parent::__construct(false, $name = 'Vets Locations');	
    }
    function widget($args, $instance) {
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
        ?>
              <?php echo $before_widget; ?>
                  <?php if ( $title )
                        echo $before_title . $title . $after_title; ?>
				<?php $posts = get_posts(array('post_type'=>'location')); ?>
				<?php foreach($posts as $post): ?>
					<div class="post location">
						<div class="intro">
							<a href="<?php bloginfo('url'); ?>/location/<?php echo $post->post_name; ?>/" title="<?php echo $post->post_title; ?>" ><?php echo get_the_post_thumbnail($post->ID, array(160,200), array('class'=>'frame')); ?></a>
						</div>

						<div class="info">
							<strong><a href="<?php bloginfo('url'); ?>/location/<?php echo $post->post_name; ?>/"><?php echo $post->post_title; ?></a> <span class="tiny">(<?php echo get_post_meta($post->ID, 'tbvets_subtitle', true); ?>)</span></strong>
							<p class="address"><?php echo get_post_meta($post->ID,'tbvets_address1',true); ?>
							<?php if((get_post_meta($post->ID,'tbvets_address2',true))) { ?><?php echo get_post_meta($post->ID,'tbvets_address2',true); ?><?php } ?>
							<?php echo get_post_meta($post->ID,'tbvets_city',true); ?>, <?php echo get_post_meta($post->ID,'tbvets_state',true); ?>  <?php echo get_post_meta($post->ID,'tbvets_zip',true); ?> <?php $check = get_post_meta($post->ID,'tbvets_map',$true); if($check): ?><span class="map-link"><small><a href="<?php echo htmlentities(get_post_meta($post->ID,'tbvets_map',true)); ?>">(View map)</a></small></span><?php endif; ?></p>

							<p class="hours"><strong>Hours:</strong><br />
							<?php echo nl2br(strip_tags(get_post_meta($post->ID,'tbvets_hours',true))); ?></p>
						</div>
					</div>
					<?php endforeach; ?>

              <?php echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {
	$instance = $old_instance;
	$instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
        $title = esc_attr($instance['title']);
        ?>
            <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
            </label>
            </p>
        <?php
    }

} //
function vets_widgets() {
	register_widget('VetsLocationWidget');
}
add_action('widgets_init','vets_widgets');
