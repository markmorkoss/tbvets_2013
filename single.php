<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage tbvets
 */

get_header(); ?>

<?php global $post; ?>
<?php $type = $post->post_type; ?>
	<?php include( get_bloginfo('siteurl') . 'wp-content/themes/tbvets/single-' .$type .'.php') ; ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
